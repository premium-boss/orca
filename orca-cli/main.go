package main

import (
	"flag"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"time"

	"gitlab.com/premium-boss/orca/orca"
)

const (
	defaultIP   string = "127.0.0.1"
	defaultPort int    = 5050
)

func main() {
	run()
}

func run() {
	// TODO: log options (similar to client)
	ip := flag.String("ip", defaultIP, "ip address of this server")
	port := flag.Int("port", defaultPort, "rx port for this server")
	flag.Parse()
	c := make(chan os.Signal, 1) // catch signals to exit nicely
	signal.Notify(c, os.Interrupt)
	rand.Seed(time.Now().UnixNano())
	log.Println("starting Orca...")
	kill := make(chan bool)
	go orca.Run(*ip, *port, kill)
	<-c
	kill <- true
	log.Println("exiting Orca")
}
