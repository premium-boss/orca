package server

import (
	"crypto/cipher"
	"fmt"
	"log"
	"net"
	"os"
	"time"

	om "gitlab.com/premium-boss/orca/messaging"
	sig "gitlab.com/premium-boss/orca/signal"

	"github.com/google/uuid"
)

// OrcaConn constants for messaging
const (
	OrcaConnHeartbeat   int = 5
	OrcaConnTimeout     int = 10
	OrcaConnMessageRate int = 50 // every 50 ms, or 20 times a second
)

// States for usage in connection lifecycle (eventually...)
const (
	OrcaStateCreated      = 1
	OrcaStateFindingMatch = 2
	OrcaStateJoined       = 3
	OrcaStateReady        = 4
	OrcaStateLeft         = 5
)

var timeoutDur = time.Second * time.Duration(OrcaConnTimeout)
var heartBeatDur = time.Second * time.Duration(OrcaConnHeartbeat)
var messageSendDur = time.Millisecond * time.Duration(OrcaConnMessageRate)

// Blowhole is a channel of a Signal
type Blowhole chan sig.Signal

// OrcaConn holds state about the Player in relation to the Room or server,
// as opposed to his / her relation to game state.
// It should handle any buffering / relaying of messages to and from Room.
type OrcaConn struct {
	WhaleID    uint16                 // assigned ID of Whale, e.g. Player 1, Player 2, etc.
	SessionID  uuid.UUID              // ID of connection as assigned by Server
	ServerKey  string                 // basically addr and id
	UDPConn    *net.UDPConn           // UDP connection to send to client
	RemoteAddr *net.UDPAddr           // address of the client
	SymKey     cipher.Block           // symmetric key used for signing once connection is established
	PkTrk      om.PacketTracker       // tracks all packet, message, and ack info
	IsSwimming bool                   // flag for run loop
	PodChan    chan<- *om.OrcaMessage // Send a message to the Pod
	ServerChan chan<- sig.Signal      // Send a signal to the server
	OceanChan  chan<- Blowhole        // Send our own signal chan to the Ocean, to find a Pod
	Baleen     <-chan *om.OrcaPacket  // Receive a packet from the client (ultimately from server)
	Spout      Blowhole               // Receive a message from the room
	State      int                    // State tracking for internal use in the OrcaConn
	logger     *log.Logger
}

// NewOrcaConn initializes a new OrcaConn.
// TODO: verbose option, logfile, or ioutil.Discard, e.g.
func NewOrcaConn(serverKey string, id uuid.UUID, addr *net.UDPAddr, conn *net.UDPConn, sCh chan<- sig.Signal, bnCh <-chan *om.OrcaPacket) (*OrcaConn, error) {
	// TODO: key []byte
	// k, err := aes.NewCipher(key) // aes 256, assuming key is 32 bytes
	// if err != nil {
	// 	return nil, err
	// }
	o := OrcaConn{
		WhaleID:    65535,
		SessionID:  id,
		ServerKey:  serverKey,
		UDPConn:    conn,
		RemoteAddr: addr,
		//SymKey:     k,
		PkTrk:      om.NewPacketTracker(),
		IsSwimming: false,
		Baleen:     bnCh,
		Spout:      make(chan sig.Signal, 256), // TODO: const, better. this is our forwarder!
		ServerChan: sCh,
		State:      OrcaStateCreated,
	}
	pre := fmt.Sprintf("[orca_conn:%s]", o.SessionID.String()[:5])
	o.logger = log.New(os.Stdout, pre, log.LstdFlags)
	return &o, nil
}

// Swim represents a run loop for an Orca
func (c *OrcaConn) Swim(oCh chan<- Blowhole) {
	c.logger.Printf("starting to swim")
	c.OceanChan = oCh
	c.IsSwimming = true
	timeout := time.NewTimer(timeoutDur)
	heartTick := time.NewTicker(heartBeatDur) // send heartbeats to client
	singTick := time.NewTicker(messageSendDur)
	for c.IsSwimming {
		select {
		case pk := <-c.Baleen:
			om.ResetTimer(timeout, &timeoutDur)
			c.receivePacket(pk)
		case s := <-c.Spout:
			c.handleSignal(s)
		case <-singTick.C:
			c.sing()
		case <-heartTick.C:
			c.queueHeartbeat()
		case <-timeout.C:
			c.logger.Printf("time out: stopping...")
			singTick.Stop()
			c.stop()
		}
	}
}

// queueHeartbeat queues up a heartbeat message
func (c *OrcaConn) queueHeartbeat() {
	em := om.EmptyMessage{}
	h := om.NewOrcaMessage(om.TagHeartbeat, &em)
	c.logger.Printf("queing heartbeat")
	c.PkTrk.QueueMessage(h)
}

// handleMessage validates and applies a message
func (c *OrcaConn) handleMessage(msg *om.OrcaMessage) {
	switch msg.Tag {
	case om.TagFindMatch:
		c.logger.Println("recv TagFindMatch, forwarding to the ocean")
		if c.State < OrcaStateFindingMatch {
			c.State = OrcaStateFindingMatch
			c.OceanChan <- c.Spout // TODO: some sort of reply?
		}
	case om.TagConfirmJoined:
		c.logger.Println("recv TagConfirmJoined")
		wj := msg.Body.(*om.WhaleConfirmJoined)
		if wj.ID == c.WhaleID {
			c.logger.Printf("joined for me is valid: relaying confirmation to Pod: %d\n", wj.ID)
			c.State = OrcaStateJoined
			c.tryRelayToPod(msg)
		}
	case om.TagReady:
		c.logger.Println("recv TagReady")
		wr := msg.Body.(*om.WhaleReady)
		if wr.ID == c.WhaleID {
			c.State = OrcaStateReady
			c.tryRelayToPod(msg)
		}
	case om.TagLeft:
		c.logger.Println("recv TagLeft")
		c.State = OrcaStateLeft
		c.PodChan <- msg
		// TODO: nil it out?
	case om.TagStopGame:
		c.logger.Println("recv TagStopGame")
		// TODO:
	case om.TagHeartbeat:
		// pass
		c.logger.Printf("recv TagHeartbeat from %s", c.SessionID)
	default:
		if msg.Tag > om.Reserved {
			c.tryRelayToPod(msg) // TODO: check state here?
		} else {
			c.logger.Println("unrecognized message type, ignoring...", msg.Tag)
		}
	}
}

func (c *OrcaConn) handleSignal(s sig.Signal) {
	switch s.Sig {
	case sig.SigOrcaAccepted:
		oa := s.Data.(sig.OrcaAccepted)
		c.WhaleID = oa.ID
		c.PodChan = oa.PodChan
		wj := om.WhaleJoined{ID: c.WhaleID}
		c.logger.Printf("OrcaAccepted, updating PodChan, queing a WhaleJoined with player ID %d...", c.WhaleID)
		msg := om.NewOrcaMessage(om.TagJoined, &wj)
		c.PkTrk.QueueMessage(msg)
	case sig.SigSend:
		// queue up to relay to client
		msg := s.Data.(*om.OrcaMessage)
		c.PkTrk.QueueMessage(msg) // TODO: add to queue
	case sig.SigKill:
		// TODO:
	}
}

func (c *OrcaConn) receivePacket(pk *om.OrcaPacket) {
	msgs := c.PkTrk.ReceivePacket(pk) // process messages, returns what we can handle
	for _, msg := range msgs {
		c.handleMessage(msg)
	}
}

func (c *OrcaConn) tryRelayToPod(msg *om.OrcaMessage) {
	if c.PodChan != nil {
		c.PodChan <- msg
	}
}

// sing sends out next messages
func (c *OrcaConn) sing() {
	send, b, err := c.PkTrk.CreateNextPacket(c.SessionID)
	if err != nil {
		c.logger.Printf("ERROR creating packet %d", c.PkTrk.TxSequence-1)
		c.logger.Println(err)
		return
	}
	if !send {
		return // no messages
	}
	_, err = c.UDPConn.WriteToUDP(b, c.RemoteAddr)
	if err != nil {
		c.logger.Printf("ERROR sending packet %d", c.PkTrk.TxSequence-1)
		c.logger.Println(err)
	}
}

// stop stops the connection and releases resources
func (c *OrcaConn) stop() {
	c.IsSwimming = false
	if c.PodChan != nil {
		wl := om.WhaleLeft{ID: c.WhaleID}
		msg := om.NewOrcaMessage(om.TagLeft, &wl)
		c.PodChan <- msg
	}
	// signal to server
	s := sig.Signal{Sig: sig.SigOrcaDisconnect, Data: c.ServerKey}
	c.ServerChan <- s
	c.logger.Printf("stopped")
	// TODO: make sure we clean and close channels properly, but DO NOT DEADLOCK
	c.PodChan = nil
	c.ServerChan = nil
	c.Baleen = nil
}
