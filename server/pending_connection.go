package server

import "time"

// PendingConnection holds state about a pending connection during handshake.
// May hold keys, etc.
type PendingConnection struct {
	ServerSalt int64 // used to challenge the client
	ClientSalt int64
	Complete   bool
}

// PendingTimeoutCheck records when to remove a pending connection.
type PendingTimeoutCheck struct {
	Expiry time.Time
	Key    string
}
