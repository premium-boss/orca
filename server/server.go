package server

import (
	"errors"
	"fmt"
	"log"
	"math/rand"
	"net"
	"os"
	"sync"
	"time"

	om "gitlab.com/premium-boss/orca/messaging"
	sig "gitlab.com/premium-boss/orca/signal"

	"github.com/google/uuid"
)

const (
	bufSize                 int = 2056
	maxClients              int = 1000 // initial goal
	connPacketBufSize       int = 100
	connectionRequestLength int = 1024 // padded length of connection req
	// TODO max pending?
)

// Server receives connections and messages.
// It tracks clients.
// TODO: replace w/ sync.Map in golang 1.9, atomics should scale better than the RWMutex
type Server struct {
	ConnectionLock   sync.RWMutex
	PendingLock      sync.RWMutex
	Connections      map[string]chan *om.OrcaPacket
	Pending          map[string]*PendingConnection
	PendingCheckChan chan PendingTimeoutCheck
	SigChan          chan sig.Signal
	OceanChan        chan<- Blowhole
	CancelServe      chan bool
	CancelPending    chan bool
	Conn             *net.UDPConn
	logger           *log.Logger
}

// NewServer creates a new game server
func NewServer(oCh chan<- Blowhole) *Server {
	s := Server{
		Connections:      make(map[string]chan *om.OrcaPacket),
		Pending:          make(map[string]*PendingConnection),
		PendingCheckChan: make(chan PendingTimeoutCheck, 1000), // TODO: constant
		SigChan:          make(chan sig.Signal, 100),
		OceanChan:        oCh,
		CancelServe:      make(chan bool, 1),
		CancelPending:    make(chan bool, 1),
	}
	s.logger = log.New(os.Stdout, "[server]", log.LstdFlags)
	return &s
}

// Serve starts the server and listens for incoming signals
func (s *Server) Serve(ip string, port int) {
	s.Conn = s.connectOrDie(ip, port)
	go s.checkPending(s.CancelPending, s.PendingCheckChan)
	go s.serveMessages()
Loop:
	for {
		sg := <-s.SigChan
		switch sg.Sig {
		case sig.SigOrcaDisconnect:
			key := sg.Data.(string)
			s.logger.Println("received disconnect: removing...", key)
			s.removeConnection(key)
		case sig.SigKill:
			s.CancelServe <- true
			s.CancelPending <- true
			break Loop
		}
	}
}

// Stop stops the server.
func (s *Server) Stop() {
	s.SigChan <- sig.Kill()
}

// ServeMessages handles incoming game messages for connected players
// TODO: try to intercept signals for graceful cancelling?
// TODO: offer multiple ports in case one is used?
// TODO: guarantee message conn before we start Accepting Connections!
func (s *Server) serveMessages() {
	s.logger.Println("ORCA server starting to serve messages...")

	// handle packets until quitting
	// TODO: pass in a cancel chan
	for {
		err := s.receiveMessage()
		if err != nil {
			s.logger.Println("error reading msg: ", err) // TODO: should we fatal?
		}
	}
}

// connectOrDie attempts to create a UDP connection at the configured ip and
// port, and fatals if it cannot.
func (s *Server) connectOrDie(ip string, port int) *net.UDPConn {
	addr := om.BuildAddr(ip, port)
	udpAddr, err := net.ResolveUDPAddr("udp", addr)
	if err != nil {
		s.logger.Fatalln("error resolving udp addr", err)
	}
	conn, err := net.ListenUDP("udp", udpAddr)
	if err != nil {
		s.logger.Fatalln("error listening on udp addr", err)
	}
	s.logger.Println("listening for UDP messages at:", addr)
	return conn
}

// receiveMessage reads a single message from the server's UDP connection and
// starts a goroutine to process it.
func (s *Server) receiveMessage() error {
	buf := make([]byte, bufSize)
	rd, addr, err := s.Conn.ReadFromUDP(buf)
	if err != nil {
		fmt.Println(err)
		return err
	}
	go s.handlePacket(buf[:rd], addr) // TODO: should we queue this or spawn each?
	return nil
}

// handlePacket handles a packet sent to the server.
func (s *Server) handlePacket(buf []byte, addr *net.UDPAddr) {
	pk := om.OrcaPacket{}
	_, err := pk.UnmarshalBinary(buf) // parse packet header
	if err != nil {
		s.logger.Println("bad packet unmarshal", err)
		return // invalid overall packet
	}
	if pk.SessionID != om.BlankID {
		s.handleExistingClient(addr, &pk) // should be an existing client
	} else {
		s.logger.Println("trying client handshake")
		s.handshake(&pk, addr, buf) // this is a new client: attept handshake
	}
}

// checkPending removes pending conns that timed out, or have finished.
func (s *Server) checkPending(kill <-chan bool, cs <-chan PendingTimeoutCheck) {
	var nxt PendingTimeoutCheck
	deleted := false
Loop:
	for {
		select {
		case nxt = <-cs:
			remaining := time.Until(nxt.Expiry)
			if remaining > 0 {
				time.Sleep(remaining) // wait until it expires
			}
			s.deletePendingConn(nxt.Key)
			deleted = true
		case <-kill:
			break Loop
		default:
		}
		if !deleted {
			// if we deleted, try to process next immediately. Otherwise wait.
			time.Sleep(time.Second * time.Duration(5)) // TODO: constant
		}
		deleted = false
	}
}

// handleExistingClient handles a packet intended for a connected client.
func (s *Server) handleExistingClient(addr *net.UDPAddr, pk *om.OrcaPacket) {
	key := buildKey(addr, pk.SessionID)
	s.ConnectionLock.RLock()
	ch, ok := s.Connections[key]
	s.ConnectionLock.RUnlock()
	if !ok {
		s.logger.Println("bad or expired client:", pk.SessionID)
		return
	}
	if !pk.IsValidChecksum() {
		s.logger.Println("bad checksum")
		return // invalid packet
	}
	if err := pk.UnmarshalBody(); err != nil {
		s.logger.Println("error unmarshalling body")
		return // bad messages
	}
	ch <- pk // send to the real client handler
}

// handshake attempts to follow connection protocol, bailing if client does not
// follow suit. We expect a real connection request to have a CRC32, a
// connection request enum, a client salt, and padding up to 1k.
// If this is not met, we can throw out.
// NOTE: we've only deserialized the header to this point.
func (s *Server) handshake(pk *om.OrcaPacket, addr *net.UDPAddr, buf []byte) {
	if len(buf) < connectionRequestLength {
		s.logger.Println("handshake request: length too short")
		return // bail: too short: this is to prevent DDOS amplification attack
	}
	if !pk.IsValidChecksum() {
		s.logger.Println("handshake request: bad checksum")
		return // bad checksum
	}
	if pk.MessageCount != 1 {
		s.logger.Println("handshake request: bad message count")
		return // bad message count
	}
	cr, ok := validateConnReq(pk)
	if ok {
		s.procConnReq(cr, addr)
		return
	}
	ccr, ok := validateConnChallResp(pk)
	if ok {
		s.procChallResp(ccr, addr)
	}
	s.logger.Println("handshake: bad msg on new connection")
}

// procConnReq processes a connection request message. It creates or retrieves
// a PendingConnection, whch we use to send a connection challenge.
func (s *Server) procConnReq(cr *om.ConnectionRequest, addr *net.UDPAddr) {
	pc := s.pendingConn(addr, cr.ClientSalt) // retrieve or create
	s.sendChallenge(addr, pc)                // send challenge
}

// procChallResp processes a connection challenge response. If we still have
// a PendingConnection for this challenge and the server salt is valid, we can
// accept the new client.
// TODO: key exchange should be next step.
func (s *Server) procChallResp(ccr *om.ConnChallResp, addr *net.UDPAddr) {
	key := buildPendingKey(addr, ccr.ClientSalt)
	s.PendingLock.Lock()
	pc, ok := s.Pending[key]
	if !ok {
		return // no conn, could be old
	}
	if pc.ServerSalt != ccr.ServerSalt {
		return // bad salt, TODO: log?
	}
	if pc.Complete {
		return // already accepting or accepted
	}
	pc.Complete = true // assuming this worked...
	s.PendingLock.Unlock()
	s.accept(addr, pc) // TODO: do we want to handle error here?
}

// pendingConn creates or retrieves a pending connection from the map.
// If one already exists, we use the same server salt in the challenge.
// If we create a new one, we must queue up its timeout and removal.
func (s *Server) pendingConn(addr *net.UDPAddr, salt int64) *PendingConnection {
	key := buildPendingKey(addr, salt)
	s.PendingLock.Lock()
	defer s.PendingLock.Unlock()
	pc, ok := s.Pending[key]
	if !ok {

		pc = &PendingConnection{
			ClientSalt: salt,
			ServerSalt: rand.Int63(),
			Complete:   false,
		}
		s.logger.Println("creating new pendingConn for:", addr.String(), salt, "serverSalt set:", pc.ServerSalt)
		s.Pending[key] = pc
		exp := time.Now().Add(time.Duration(5) * time.Second) // TODO: constant
		chk := PendingTimeoutCheck{Expiry: exp, Key: key}
		s.PendingCheckChan <- chk // queue a timeout
	} else {
		s.logger.Println("retrieved pendingConn for:", addr.String(), salt)
	}

	return pc
}

// deletePendingConn deletes a PendingConnection from the map.
func (s *Server) deletePendingConn(key string) {
	s.PendingLock.Lock()
	defer s.PendingLock.Unlock()
	delete(s.Pending, key) // safe even if key doesn't exist
}

// sendChallenge sends a connection challenge message to the client.
func (s *Server) sendChallenge(addr *net.UDPAddr, pc *PendingConnection) {
	cc := om.ConnectionChallenge{
		ClientSalt: pc.ClientSalt,
		ServerSalt: pc.ServerSalt,
	}
	s.logger.Println("sending challeng with clientSalt:", cc.ClientSalt, "serverSalt:", cc.ServerSalt)
	msg := om.NewOrcaMessage(om.TagConnectionChallenge, &cc)
	b, err := msg.MarshalBinary()
	if err != nil {
		// TODO: handle better?
		s.logger.Println("error marshalling connection challenge...", err)
		return
	}
	cgPk := om.NewOrcaPacket(om.BlankID, 0, 0, 1, 0, b)
	data, err := cgPk.MarshalBinary()
	if err != nil {
		// TODO: handle better?
		s.logger.Println("error marshalling cc packet..", err)
		return
	}
	_, err = s.Conn.WriteToUDP(data, addr)
	if err != nil {
		// TODO: handle better?
		s.logger.Println("error sending cc packet..", err)
		return
	}
}

// accept accepts a client and creates an OrcaConn for it.
func (s *Server) accept(addr *net.UDPAddr, pc *PendingConnection) error {
	oc, err := s.assignConnection(addr)
	if err != nil {
		return err
	}
	wc := om.NewWhaleConnected(oc.SessionID)
	msg := om.NewOrcaMessage(om.TagConnected, wc)
	sg := sig.Signal{Sig: sig.SigSend, Data: msg}
	oc.Spout <- sg
	// TODO: check err? change to a code?
	// TODO: if at capacity, we should still send something back to the client so they can try another server

	return err
}

// assignConnection creates and stores a new OrcaConn for a client, if there is
// room.
func (s *Server) assignConnection(addr *net.UDPAddr) (*OrcaConn, error) {
	id, err := uuid.NewUUID()
	if err != nil {
		return nil, err
	}
	key := buildKey(addr, id)
	bCh := make(chan *om.OrcaPacket, connPacketBufSize)
	s.ConnectionLock.Lock()
	defer s.ConnectionLock.Unlock()
	if len(s.Connections) >= maxClients {
		return nil, errors.New("at capacity")
	}
	oc, err := NewOrcaConn(key, id, addr, s.Conn, s.SigChan, bCh)
	if err != nil {
		return nil, err
	}
	s.Connections[key] = bCh // assign the slot
	go oc.Swim(s.OceanChan)  // start the handler TODO: consider doing this after handshake...
	return oc, nil
}

// removeConnection removes a connection from the map.
func (s *Server) removeConnection(key string) {
	s.ConnectionLock.Lock()
	defer s.ConnectionLock.Unlock()
	s.logger.Println("removing client:", key)
	delete(s.Connections, key)
}

// validateConnReq validates a ConnectionRequest.
func validateConnReq(pk *om.OrcaPacket) (*om.ConnectionRequest, bool) {
	msg := om.OrcaMessage{}
	err := msg.UnmarshalIf(pk.Payload, om.TagConnectionRequest)
	if err != nil {
		// TODO: log?
		return nil, false // bad connection request: tag, etc.
	}
	cr := msg.Body.(*om.ConnectionRequest)
	if cr.ClientSalt == 0 {
		// TODO: log?
		return nil, false // bad client salt
	}
	return cr, true
}

// validateConnChallResp calidate a ConnChallResp.
func validateConnChallResp(pk *om.OrcaPacket) (*om.ConnChallResp, bool) {
	msg := om.OrcaMessage{}
	err := msg.UnmarshalIf(pk.Payload, om.TagConnectionChallengeResponse)
	if err != nil {
		// TODO: log?
		return nil, false // bad challenge response: tag, etc.
	}
	ccr := msg.Body.(*om.ConnChallResp)
	if ccr.ClientSalt == 0 || ccr.ServerSalt == 0 {
		// TODO: log?
		return nil, false // bad salts
	}
	return ccr, true
}

// buildKey builds a key out of an address and a uuid, for mapping a
// connection within the server.
func buildKey(addr *net.UDPAddr, id uuid.UUID) string {
	return fmt.Sprintf("%s:%s", addr.String(), id.String())
}

// buildPendingKey builds a key out of an address and a client salt, for mapping
// a pending connection within the server.
func buildPendingKey(addr *net.UDPAddr, salt int64) string {
	return fmt.Sprintf("%s:%d", addr.String(), salt)
}
