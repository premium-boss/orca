package server

import (
	"log"
	"os"

	pd "gitlab.com/premium-boss/orca/pod"
	sig "gitlab.com/premium-boss/orca/signal"

	uuid "github.com/google/uuid"
)

// Ocean constants
const (
	MaxPods  int = 250
	MaxJoins int = 1000
)

// Ocean holds all Pods and processes join requests in a queue
type Ocean struct {
	QueueChan   chan Blowhole
	sigChan     chan sig.Signal
	waitingPods []*pd.Pod
	fullPods    map[uuid.UUID]*pd.Pod
	logger      *log.Logger
}

// NewOcean creates a new Ocean
func NewOcean() *Ocean {
	o := Ocean{
		QueueChan:   make(chan Blowhole, MaxJoins),
		sigChan:     make(chan sig.Signal, MaxPods),
		waitingPods: make([]*pd.Pod, 0, MaxPods),
		fullPods:    make(map[uuid.UUID]*pd.Pod),
	}
	o.logger = log.New(os.Stdout, "[ocean]", log.LstdFlags)
	return &o
}

// Flow runs the Ocean
func (o *Ocean) Flow() {
	o.logger.Println("starting to flow")
	reply := make(chan bool, 1)
Loop:
	for {
		select {
		case bh := <-o.QueueChan:
			o.logger.Println("matchmaking whale")
			res := o.matchmakeWhale(reply, bh)
			if !res {
				o.logger.Println("all pods at capacity") // TODO: cooldown or leave to server?
			}
		case s := <-o.sigChan:
			o.logger.Println("received sig:", s.Sig)
			switch s.Sig {
			case sig.SigPodLostPlayer:
				o.logger.Println("pod lost player")
				id := s.Data.(uuid.UUID)
				o.restorePodToQueue(id)
			case sig.SigPodDone:
				id := s.Data.(uuid.UUID)
				o.podDone(id)
			case sig.SigKill:
				o.kill(s)
				break Loop
			}
		}
	}
	o.logger.Println("flow is over")
}

// Stop stops the Ocean flow.
func (o *Ocean) Stop() {
	o.sigChan <- sig.Kill()
}

// matchmakeWhale finds a room for incoming connections.
func (o *Ocean) matchmakeWhale(reply chan bool, bh chan sig.Signal) bool {
	ta := sig.TryAccept{Blowhole: bh, ReplyChan: reply} // really, we just need the channels...
	s := sig.Signal{Sig: sig.SigTryAccept, Data: ta}
	nw := len(o.waitingPods)
	nf := len(o.fullPods)
	placed := false

	// if pod available, send a signal
	if nw > 0 {
		i := nw - 1
		pod := o.waitingPods[i]
		pod.SigChan <- s
		placed = <-reply
		if !placed {
			o.removeWaitingPodAt(i)
			o.fullPods[pod.ID] = pod // move the old one as it is full
		}
	}

	// create a new pod and try again if needed
	max := nw+nf == MaxPods
	if !placed && !max {
		o.logger.Println("creating new pod")
		np := pd.NewPod(uuid.New(), o.sigChan)
		o.waitingPods = append(o.waitingPods, np)
		go np.Swim() // start the pod
		np.SigChan <- s
		placed = <-reply
	} else if max {
		// TODO: signal out, we will need a service discovery mechanism, report full, then report empty again
		// TODO: send signal out to server?, we're full! (maybe not needed, but it'd be nice to pick a free server before establishing conn)
		// TODO: send signal to the connection, we're full! needs to try another server
	}
	// TODO: check placed, error?
	return placed
}

func (o *Ocean) restorePodToQueue(id uuid.UUID) {
	pod, ok := o.fullPods[id]
	if ok {
		o.waitingPods = append(o.waitingPods, pod) // move to front of queue
	}
}

// podDone removed Pod when finished. Technically, there is a chance pod is last in queue
// or even earlier if it completed before any other player attempts to join.
// That branch is semi-expensive, but queue is small and this should be rare.
func (o *Ocean) podDone(id uuid.UUID) {
	_, ok := o.fullPods[id]
	if ok {
		delete(o.fullPods, id) // pod should be here
	} else {
		for i, p := range o.waitingPods {
			if p.ID == id {
				o.removeWaitingPodAt(i)
				break
			}
		}
	}
}

func (o *Ocean) removeWaitingPodAt(i int) {
	o.waitingPods[i] = nil // NOTE: this is important, or the whale will not be GC'd
	o.waitingPods = append(o.waitingPods[:i], o.waitingPods[i+1:]...)
}

func (o *Ocean) kill(k sig.Signal) {
	for _, wp := range o.waitingPods {
		wp.SigChan <- k
	}
	for _, fp := range o.fullPods {
		fp.SigChan <- k
	}
}
