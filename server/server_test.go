package server

import (
	"fmt"
	"net"
	"testing"
)

func TestServerHandlesPackets(t *testing.T) {
	// make a udp client, make sure read works as expected
	// later, we can make a test route to preserve this, need to set something checkable

	// capture logging

	// start a server
	ip := "127.0.0.1"
	port := 5000
	go Run(ip, port)

	// create addr
	addr, err := net.ResolveUDPAddr("udp", fmt.Sprintf("%s:%d", ip, port))
	if err != nil {
		t.Error("Error resolving addr:", addr)
	}

	// connect client
	client, err := net.DialUDP("udp", nil, addr)
	if err != nil {
		t.Error("Error dialing UDP:", err)
	}
	defer client.Close()

	// send msg
	msg := []byte("This is a message from a client!!!")
	_, err = client.Write(msg)
	if err != nil {
		t.Error("Error sending msg:", err)
	}

	// TODO: expect a msg back!

}
