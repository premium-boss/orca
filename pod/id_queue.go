package pod

// idQueue is a simple queue for ID ints
// so we can easily assign as whales enter and leave
// the pod
type idQueue struct {
	ids []uint16
}

func newIDQueue(max int) idQueue {
	q := idQueue{ids: make([]uint16, max, max)}
	for i := 0; i < max; i++ {
		q.ids[i] = uint16(i)
	}
	return q
}

func (q *idQueue) push(i uint16) {
	q.ids = append(q.ids, i)
}

func (q *idQueue) pop() uint16 {
	i := q.ids[0]
	q.ids = q.ids[1:]
	return i
}

func (q *idQueue) len() int {
	return len(q.ids)
}
