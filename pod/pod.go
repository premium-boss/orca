package pod

import (
	"fmt"
	"log"
	"os"
	"time"

	gm "gitlab.com/premium-boss/orca/game"
	om "gitlab.com/premium-boss/orca/messaging"
	sig "gitlab.com/premium-boss/orca/signal"

	uuid "github.com/google/uuid"
)

// Constants for room size and timing
const (
	RoundTime   int = 10 // RoundTime is the time of a game round in minutes
	JoinTimeout int = 5  // seconds to respond to the joined call
)

// Pod is a game room for managing a single game instance.
type Pod struct {
	ID              uuid.UUID
	StartTime       time.Time
	GameStarted     bool
	Whales          map[uint16]*Whale
	OrcaChan        chan *om.OrcaMessage
	SigChan         chan sig.Signal
	gameChan        chan gm.GameMessage
	isSwimming      bool
	countdownCancel chan bool
	oceanChan       chan<- sig.Signal
	idQ             idQueue
	logger          *log.Logger
	game            gm.Game
}

// NewPod initializes a new Pod.
func NewPod(id uuid.UUID, oCh chan<- sig.Signal) *Pod {
	p := Pod{
		ID:              id,
		GameStarted:     false,
		Whales:          make(map[uint16]*Whale, gm.MaxPlayers),
		OrcaChan:        make(chan *om.OrcaMessage, 100), // TODO:... use smarter constants?
		SigChan:         make(chan sig.Signal, gm.MaxPlayers+4),
		gameChan:        make(chan gm.GameMessage, 1256), // Channel for bcast, TODO: cap? length?
		isSwimming:      false,
		countdownCancel: make(chan bool, 1),
		oceanChan:       oCh, // signal to outside world that this has finished
		idQ:             newIDQueue(gm.MaxPlayers),
	}
	pre := fmt.Sprintf("[pod:%s]", id.String()[:8])
	p.logger = log.New(os.Stdout, pre, log.LstdFlags)
	return &p
}

// Swim is the main message loop of the Pod / room.
func (p *Pod) Swim() {
	p.isSwimming = true
	for p.isSwimming {
		select {
		case msg := <-p.OrcaChan:
			p.handleMessage(msg)
		case s := <-p.SigChan:
			p.handleSignal(s)
		case gmsg := <-p.gameChan:
			if gmsg.IsBroadcast {
				p.broadcast(gmsg.Message)
			} else {
				p.send(gmsg.PlayerID, gmsg.Message)
			}
		}
	}
}

// handleMessage handles a message
func (p *Pod) handleMessage(msg *om.OrcaMessage) {
	switch msg.Tag {
	case om.TagReady:
		wr := msg.Body.(*om.WhaleReady)
		p.whaleReady(wr.ID)
		p.logger.Println("whale ready:", wr.ID)
	case om.TagConfirmJoined:
		wj := msg.Body.(*om.WhaleConfirmJoined)
		p.whaleConfirmJoined(wj.ID) // broadcast to other players
		p.logger.Println("whale confirm joined:", wj.ID)
	case om.TagLeft:
		wl := msg.Body.(*om.WhaleLeft)
		p.logger.Println("whale left:", wl.ID)
		p.whaleLeft(wl.ID)
	default:
		if msg.Tag > om.Reserved && p.GameStarted {
			p.game.HandleMessage(msg)
		}
	}
}

// handleSignal handles a signal
func (p *Pod) handleSignal(s sig.Signal) {
	switch s.Sig {
	case sig.SigTryAccept:
		ta := s.Data.(sig.TryAccept)
		p.accept(ta)
	case sig.SigCheckJoined:
		id := s.Data.(uint16)
		p.logger.Println("recv SigCheckJoined", id)
		w := p.Whales[id]
		if w.State == WhaleStateAcceptedPendingJoin {
			p.logger.Println("ensure join: state still AcceptedPending for id:", id)
			p.whaleLeft(id)
			// TODO: apply to game
		}
	case sig.SigKill:
		p.stopGame()

	}
}

// accept attempts to accept a new member into the pod.
// Another Whale could have beaten you to it..
// TODO: we could have separate logic for if a game has started and needs a player...
func (p *Pod) accept(ta sig.TryAccept) {
	if p.isFull() {
		ta.ReplyChan <- false
	} else {
		ta.ReplyChan <- true
		id := p.idQ.pop()
		p.logger.Printf("accepted whale, playerID: %d\n", id)
		w := NewWhale(id)
		w.State = WhaleStateAcceptedPendingJoin
		w.Blowhole = ta.Blowhole
		p.Whales[id] = w
		oa := sig.NewOrcaAccepted(id, p.OrcaChan)
		s := sig.Signal{Sig: sig.SigOrcaAccepted, Data: oa}
		w.Blowhole <- s                // notify joined
		go ensureJoined(p.SigChan, id) // make sure the OrcaConn acknowledges the Join, or else boot
	}
}

// broadcast sends a specific message to the players to relay
func (p *Pod) broadcast(msg *om.OrcaMessage) {
	// TODO:!!! we must copy a message when broadcasting!!!
	// not only to protect concurrent access, but because packet tracker
	// depends on it!
	// for now, only doing a shallow copy, but see if we need deep...
	for _, w := range p.Whales {
		m := *msg
		sg := sig.Signal{Sig: sig.SigSend, Data: &m}
		w.Blowhole <- sg
	}
}

func (p *Pod) send(id uint16, msg *om.OrcaMessage) {
	w, ok := p.Whales[id]
	if !ok {
		return
	}
	m := *msg // just to be safe
	sg := sig.Signal{Sig: sig.SigSend, Data: &m}
	w.Blowhole <- sg
}

// whaleConfirmJoined confirms the join of a whale to the pod
func (p *Pod) whaleConfirmJoined(id uint16) {
	w, ok := p.Whales[id]
	if ok && w.State == WhaleStateAcceptedPendingJoin {
		w.State = WhaleStateJoined
	}
	wj := om.WhaleConfirmJoined{ID: id}
	msg := om.NewOrcaMessage(om.TagConfirmJoined, &wj)
	p.broadcast(msg)
}

// whaleReady confirms whale is ready to start. Updates state.
// If all whales ready, we start the game
// TODO: handle joining of a started game, if we decide to allow this
func (p *Pod) whaleReady(id uint16) {
	w, ok := p.Whales[id]
	if ok && w.State == WhaleStateJoined {
		w.State = WhaleStateReady
	}
	wr := om.WhaleReady{ID: id}
	msg := om.NewOrcaMessage(om.TagReady, &wr)
	p.broadcast(msg)
	if !p.GameStarted && p.allReady() {
		p.logger.Println("all ready, starting game!")
		p.startGame()
	}
}

// allReady checks if we have a full pod of ready whales
// TODO: we could let the "leader" start the game?
func (p *Pod) allReady() bool {
	if len(p.Whales) != gm.MaxPlayers {
		return false
	}

	for _, w := range p.Whales {
		if w.State != WhaleStateReady {
			return false
		}
	}
	return true
}

// whaleLeft removes a whale from the map
func (p *Pod) whaleLeft(id uint16) {
	delete(p.Whales, id)
	p.idQ.push(id)
	wl := om.WhaleLeft{ID: id}
	msg := om.NewOrcaMessage(om.TagLeft, &wl)
	p.broadcast(msg)
	if p.GameStarted && len(p.Whales) == 0 {
		// the last player has left
		p.stopGame()
	} else if !p.GameStarted && len(p.Whales) == gm.MaxPlayers-1 {
		// If we've lost a player but we've previously finished matchmaking, we need
		// to alert the world that we require a new player again.
		// NOTE: we may also want to consider if started but young, but this can be added later
		p.oceanChan <- sig.Signal{Sig: sig.SigPodLostPlayer, Data: p.ID}
	}
}

// startGame starts the game
func (p *Pod) startGame() {
	if gm.Spawn == nil {
		panic("no game!")
	}
	p.game = gm.Spawn() // NOTE: will error if null?
	p.game.Init(p.gameChan)
	np := len(p.Whales)
	ids := make([]int, np, np)
	for i, w := range p.Whales {
		ids[i] = int(w.ID)
	}
	go p.game.Start(ids)
	p.GameStarted = true
	p.StartTime = time.Now().UTC()
	sg := om.StartGame{UnixTime: p.StartTime.Unix()}
	sg.EncodeIDs(ids)
	// TODO: consider letting game do this?
	msg := om.NewOrcaMessage(om.TagStartGame, &sg)
	p.broadcast(msg)
}

// stopGame stops the game.
// NOTE: this could be called even if game hasn't started.
// Be prepared.
// TODO: whatever we want to do on win, e.g. stats, rematch, etc.
func (p *Pod) stopGame() {
	p.logger.Println("stopping game")
	p.isSwimming = false
	sg := om.StopGame{}
	msg := om.NewOrcaMessage(om.TagStopGame, &sg)
	p.broadcast(msg)
	p.oceanChan <- sig.Signal{Sig: sig.SigPodDone, Data: p.ID}
	if p.GameStarted {
		p.game.Stop()
	}
	// TODO: consider game chan??
}

// ensureJoined waits the duration of a timeout before sending a message to the Pod.
// If the OrcaConn hasn't responsed in that time, we know that it disconnected before the join.
func ensureJoined(signal chan<- sig.Signal, id uint16) {
	time.Sleep(time.Second * time.Duration(JoinTimeout))
	signal <- sig.Signal{Sig: sig.SigCheckJoined, Data: id}
}

// countdown sends a signal when the game should end
func countdown(signal chan<- sig.Signal, cancel <-chan bool) {
	t := time.NewTimer(time.Minute * time.Duration(RoundTime))
	select {
	case <-t.C:
		signal <- sig.Signal{Sig: sig.SigGameCountdownOver, Data: nil}
	case <-cancel:
		t.Stop()
	}
}

// isFull checks if there is room in the Pod
func (p *Pod) isFull() bool {
	return len(p.Whales) == gm.MaxPlayers
}
