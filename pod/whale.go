package pod

import (
	sig "gitlab.com/premium-boss/orca/signal"
)

// Whale states
const (
	WhaleStateNone                int32 = 0
	WhaleStateAcceptedPendingJoin int32 = 1
	WhaleStateJoined              int32 = 2
	WhaleStateReady               int32 = 3
	WhaleStateSwimming            int32 = 4
	WhaleStateLeft                int32 = 5
)

// Whale represents the pod's knowledge of a connection.
// Really, this is a player ID, a channel to the connection, and some
// state tracking.
type Whale struct {
	ID       uint16
	Blowhole chan<- sig.Signal
	State    int32
}

// NewWhale creates a new Whale
func NewWhale(id uint16) *Whale {
	w := Whale{
		ID:    id,
		State: WhaleStateNone,
	}
	return &w
}
