package game

import (
	om "gitlab.com/premium-boss/orca/messaging"
)

// GameMessage is basically another signal... We could make it a true signal,
// but that would expose / force Signal package into games, which we don't want.
type GameMessage struct {
	IsBroadcast bool
	PlayerID    uint16
	Message     *om.OrcaMessage
}
