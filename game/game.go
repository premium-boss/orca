package game

import om "gitlab.com/premium-boss/orca/messaging"

// Game is an interface for any game.
// It needs to handle inputs and broadcast state.
// May need to give it an event channel.
// Otherwise, we can let the pod control the rate,
// so long as there's a Broadcast that returns what we need.
type Game interface {
	Init(broadcast chan<- GameMessage)
	HandleMessage(msg *om.OrcaMessage)
	Start(ids []int) // start w/ initial player ids
	Stop()
	Pause() // in case a player has temporarily disconnected?
	Unpause()
}

// Factory is a factory function for spawning new games
type Factory func() Game

// Spawn is a singelton-esque GameFactory instance.
// This is so we don't have to pass a Game around everywhere from some setting
// all the way down through pod, but can set once and keep extensible.
var Spawn Factory

// MaxPlayers is a settable count for how many players are allowed in a game
var MaxPlayers = 2

// TODO: var MinPlayers = 1
