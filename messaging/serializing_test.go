package messaging

import (
	"bytes"
	"encoding/binary"
	"testing"
)

var tend = binary.LittleEndian

func TestWriteInt32(t *testing.T) {
	buf := new(bytes.Buffer)
	expected := int32(197)
	WriteInt32(buf, expected)
	b := buf.Bytes()
	t.Logf("%x ", b)
	ln := len(b)
	if ln != 4 {
		t.Fatalf("Expected buffer length of 4, got %d", ln)
	}
	var actual int32
	rd := bytes.NewReader(b)
	err := binary.Read(rd, tend, &actual)
	if err != nil {
		t.Fatalf("Error on read: %v", err)
	}
	if actual != expected {
		t.Fatalf("Actual: %d does not match expected: %d", actual, expected)
	}
}

func TestWriteUInt32(t *testing.T) {
	buf := new(bytes.Buffer)
	expected := uint32(197)
	WriteUInt32(buf, expected)
	b := buf.Bytes()
	t.Logf("%x ", b)
	ln := len(b)
	if ln != 4 {
		t.Fatalf("Expected buffer length of 4, got %d", ln)
	}
	var actual uint32
	rd := bytes.NewReader(b)
	err := binary.Read(rd, tend, &actual)
	if err != nil {
		t.Fatalf("Error on read: %v", err)
	}
	if actual != expected {
		t.Fatalf("Actual: %d does not match expected: %d", actual, expected)
	}
}

func TestWriteInt64(t *testing.T) {
	buf := new(bytes.Buffer)
	expected := int64(19765004)
	WriteInt64(buf, expected)
	b := buf.Bytes()
	t.Logf("%x ", b)
	ln := len(b)
	if ln != 8 {
		t.Fatalf("Expected buffer length of 8, got %d", ln)
	}
	var actual int64
	rd := bytes.NewReader(b)
	err := binary.Read(rd, tend, &actual)
	if err != nil {
		t.Fatalf("Error on read: %v", err)
	}
	if actual != expected {
		t.Fatalf("Actual: %d does not match expected: %d", actual, expected)
	}
}

func TestWriteFloat32(t *testing.T) {
	buf := new(bytes.Buffer)
	expected := float32(-1.24567)
	WriteFloat32(buf, expected)
	b := buf.Bytes()
	t.Logf("%x ", b)
	ln := len(b)
	if ln != 4 {
		t.Fatalf("Expected buffer length of 4, got %d", ln)
	}
	var actual float32
	rd := bytes.NewReader(b)
	err := binary.Read(rd, tend, &actual)
	if err != nil {
		t.Fatalf("Error on read: %v", err)
	}
	if actual != expected {
		t.Fatalf("Actual: %f does not match expected: %f", actual, expected)
	}
}

func TestWriteFloat64(t *testing.T) {
	buf := new(bytes.Buffer)
	expected := float64(1900.7890)
	WriteFloat64(buf, expected)
	b := buf.Bytes()
	t.Logf("%x ", b)
	ln := len(b)
	if ln != 8 {
		t.Fatalf("Expected buffer length of 8, got %d", ln)
	}
	var actual float64
	rd := bytes.NewReader(b)
	err := binary.Read(rd, tend, &actual)
	if err != nil {
		t.Fatalf("Error on read: %v", err)
	}
	if actual != expected {
		t.Fatalf("Actual: %f does not match expected: %f", actual, expected)
	}
}

func TestWritePrefixedString(t *testing.T) {
	buf := new(bytes.Buffer)
	expected := "Hello, world!"
	WritePrefixedString(buf, expected)
	b := buf.Bytes()
	var ln int32
	rd := bytes.NewReader(b)
	err := binary.Read(rd, tend, &ln)
	if err != nil {
		t.Fatal("Error on length prefix read: ", err)
	}
	sbuf := make([]byte, ln)
	_, err = rd.Read(sbuf)
	if err != nil {
		t.Fatal("Error on string read: ", err)
	}
	actual := string(sbuf)
	if actual != expected {
		t.Fatalf("Actual: %s does not match expected: %s", actual, expected)
	}
}

func TestReadInt32(t *testing.T) {
	b := []byte{5, 0, 0, 0} // 5 in bytes, LittleEndian
	rd := bytes.NewReader(b)
	var actual int32
	err := ReadInt32(rd, &actual)
	if err != nil {
		t.Fatal("Error on int reader: ", err)
	}
	var expected int32 = 5
	if actual != expected {
		t.Fatalf("Actual: %d does not match expected: %d", actual, expected)
	}
}

func TestReadUInt32(t *testing.T) {
	b := []byte{5, 0, 0, 0} // 5 in bytes, LittleEndian
	rd := bytes.NewReader(b)
	var actual uint32
	err := ReadUInt32(rd, &actual)
	if err != nil {
		t.Fatal("Error on int reader: ", err)
	}
	var expected uint32 = 5
	if actual != expected {
		t.Fatalf("Actual: %d does not match expected: %d", actual, expected)
	}
}

func TestReadInt64(t *testing.T) {
	var expected int64 = 175
	b := []byte{'\xaf', 0, 0, 0, 0, 0, 0, 0} // 175 in bytes, LittleEndian
	rd := bytes.NewReader(b)
	var actual int64
	err := ReadInt64(rd, &actual)
	if err != nil {
		t.Fatal("Error on int reader: ", err)
	}
	if actual != expected {
		t.Fatalf("Actual: %d does not match expected: %d", actual, expected)
	}
}

func TestReadFloat32(t *testing.T) {
	var expected float32 = 175.889
	b := []byte{'\x96', '\xe3', '\x2f', '\x43'} // 175.889 in bytes, LittleEndian
	rd := bytes.NewReader(b)
	var actual float32
	err := ReadFloat32(rd, &actual)
	if err != nil {
		t.Fatal("Error on float read: ", err)
	}
	if actual != expected {
		t.Fatalf("Actual: %f does not match expected: %f", actual, expected)
	}
}

func TestReadFloat64(t *testing.T) {
	expected := float64(-22.222278)
	b := []byte{
		'\xcb', '\x9e', '\x04', '\x36',
		'\xe7', '\x38', '\x36', '\xc0',
	} // -22.222278 in bytes, LittleEndian
	rd := bytes.NewReader(b)
	var actual float64
	err := ReadFloat64(rd, &actual)
	if err != nil {
		t.Fatal("Error on float read: ", err)
	}
	if actual != expected {
		t.Fatalf("Actual: %f does not match expected: %f", actual, expected)
	}
}

func TestReadPrefixedString(t *testing.T) {
	expected := "Hello, world!"
	b := []byte(expected)
	ln := []byte{'\x0d', '\x00', '\x00', '\x00'} // length prefix: 13
	tot := append(ln, b...)
	t.Logf("% x", tot)
	rd := bytes.NewReader(tot)
	actual, err := ReadPrefixedString(rd)
	if err != nil {
		t.Fatal("Error on string read: ", err)
	}
	if actual != expected {
		t.Fatalf("Actual: %s does not match expected: %s", actual, expected)
	}
}
