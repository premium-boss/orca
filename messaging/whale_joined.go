package messaging

import "bytes"

// WhaleJoined is a player joined message
type WhaleJoined struct {
	ID uint16
}

// MarshalBinary writes a WhaleJoined message to bytes
func (wj *WhaleJoined) MarshalBinary() ([]byte, error) {
	buf := new(bytes.Buffer)
	err := WriteUInt16(buf, wj.ID)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// UnmarshalBinary reads a WhaleJoined message from data
func (wj *WhaleJoined) UnmarshalBinary(data []byte) (int, error) {
	rd := bytes.NewReader(data)
	err := ReadUInt16(rd, &wj.ID)
	if err != nil {
		return 0, err
	}
	return 2, nil
}
