package messaging

import "github.com/google/uuid"

// WhaleConnected is a connected message
type WhaleConnected struct {
	SessionID uuid.UUID
}

// NewWhaleConnected creates a WhaleConnected message from uuid
func NewWhaleConnected(id uuid.UUID) *WhaleConnected {
	return &WhaleConnected{SessionID: id}
}

// MarshalBinary serializes
func (w *WhaleConnected) MarshalBinary() ([]byte, error) {
	return w.SessionID.MarshalBinary()
}

// UnmarshalBinary deserializes
func (w *WhaleConnected) UnmarshalBinary(data []byte) (int, error) {
	b := [16]byte{}
	w.SessionID = uuid.UUID(b)
	err := w.SessionID.UnmarshalBinary(data[:16]) // only take 16! multiple messages!
	if err != nil {
		return 0, err
	}
	return 16, nil
}
