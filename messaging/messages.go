package messaging

import (
	"bytes"
	enc "encoding"
	"encoding/binary"
	"errors"
	"fmt"
	"hash/crc32"
	"hash/fnv"

	"github.com/google/uuid"
)

// Protocol is an implicit version tag, hashed into MagicID
const Protocol string = "ORCA_1.0"

// MagicID is an identifier for packet validation
var MagicID = []byte{'\x9c', '\x10', '\x02', '\xcd'}

// MessageUnpacker is a function that unpacks data into an OrcaMessage
type MessageUnpacker func(om *OrcaMessage, data []byte) (int, error)

// Tags for messages
const (
	TagConnected uint16 = iota + 1
	TagHeartbeat
	TagFindMatch
	TagJoined
	TagConfirmJoined
	TagReady
	TagLeft
	TagTest
	TagStartGame
	TagStopGame
	TagServerFull
	TagConnectionRequest
	TagConnectionChallenge
	TagConnectionChallengeResponse
)

// Reserved is the level until which message tags are reserved.
const Reserved uint16 = 1000

// unpackerMap maps tags to MessageUnpackers, if needed.
// NOTE: for some connection messages, we deliberately do NOT want to
// automatically deserialize them, and omit them here.
// TODO: this could just be a slice accessed by index to speed it up...
var unpackerMap = map[uint16]MessageUnpacker{
	TagHeartbeat:                   unpackHeartbeat,
	TagFindMatch:                   unpackFindMatch,
	TagJoined:                      unpackJoined,
	TagConfirmJoined:               unpackConfirmJoined,
	TagReady:                       unpackReady,
	TagLeft:                        unpackLeft,
	TagTest:                        unpackTest,
	TagStartGame:                   unpackStart,
	TagStopGame:                    unpackStop,
	TagConnected:                   unpackConnected,
	TagConnectionRequest:           unpackConnReq,
	TagConnectionChallenge:         unpackConnChall,
	TagConnectionChallengeResponse: unpackConnChallResp,
}

// RegisterMessages allows a game to register additional messages and tags.
// Tags 0 - 1000 are reserved by Orca.
func RegisterMessages(unpackers map[uint16]MessageUnpacker) {
	for k, v := range unpackers {
		if k > 1000 {
			unpackerMap[k] = v
		} else {
			err := fmt.Errorf("attempted to register message in reserved range: %d", k)
			panic(err)
		}
	}
}

// BlankID is an uninitialized SessionID
var BlankID, err = uuid.Parse("00000000-0000-0000-0000-000000000000")

/*
 * TODO: deal w/ secure handshake and signing
 * Ideally we could use some form of DTLS to connect and e0change keys,
 * as this should deal w/ the DDoS and DDoS amplification mitigations better
 * than we could.
 * This should not be handled in this module.
 */

// OrcaMarshaler specifies that a type can both marshal and unmarshal itself
// into a binary form
type OrcaMarshaler interface {
	enc.BinaryMarshaler
	UnmarshalBinary(data []byte) (int, error)
}

// OrcaPacket is a wrapper to include any message validation and security features
type OrcaPacket struct {
	Checksum     uint32
	SessionID    uuid.UUID
	Payload      []byte
	Sequence     uint16         // packet sequnce number, outgoing
	Ack          uint16         // most recent packet ID Acked
	AckBits      uint32         // if bit n is acked, then Ack - n is acked (means each ack is sent 32 times)
	MessageCount uint16         // number of messages in this packet
	Messages     []*OrcaMessage // not serialized
}

// OrcaMessage is a wrapper for all game messages
type OrcaMessage struct {
	Tag  uint16
	ID   uint16
	Body OrcaMarshaler // sub message
}

// NewOrcaPacket creates a Packet from the payload
// after generating a Checksum that includes our Magic prefix
func NewOrcaPacket(id uuid.UUID, seq, ack, ct uint16, ackBits uint32, payload []byte) OrcaPacket {
	tot := append(MagicID, payload...)
	p := OrcaPacket{
		SessionID:    id,
		Sequence:     seq,
		Checksum:     crc32.ChecksumIEEE(tot),
		Ack:          ack,
		AckBits:      ackBits,
		MessageCount: ct,
		Payload:      payload,
	}
	return p
}

// MarshalBinary serailizes the OrcaPacket.
// TODO: can we optimize this to avoid rewrite of payload to another buffer?
func (pk *OrcaPacket) MarshalBinary() ([]byte, error) {
	id, err := pk.SessionID.MarshalBinary()
	if err != nil {
		return nil, err
	}
	buf := new(bytes.Buffer)
	_, err = buf.Write(id)
	if err == nil {
		err = WriteUInt16(buf, pk.Sequence)
	}
	if err == nil {
		err = WriteUInt32(buf, pk.Checksum)
	}
	if err == nil {
		err = WriteUInt16(buf, pk.Ack)
	}
	if err == nil {
		err = WriteUInt32(buf, pk.AckBits)
	}
	if err == nil {
		err = WriteUInt16(buf, pk.MessageCount)
	}
	if err == nil {
		_, err = buf.Write(pk.Payload)
	}
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// UnmarshalBinary deserializes the OrcaPacket
func (pk *OrcaPacket) UnmarshalBinary(data []byte) (int, error) {
	// first 16 bytes should be a guid, which will default to BlankID
	ln := len(data)
	if ln < 16 {
		return 0, fmt.Errorf("bad packet length: %d", ln)
	}

	err := pk.SessionID.UnmarshalBinary(data[:16])
	if err != nil {
		return 0, err
	}

	// next 2 bytes are Sequence
	r := bytes.NewReader(data[16:])
	err = ReadUInt16(r, &pk.Sequence)
	if err != nil {
		return 0, err
	}

	// next 4 bytes should be CRC32
	err = ReadUInt32(r, &pk.Checksum)
	if err != nil {
		return 0, err
	}

	// ack 2 bytes
	err = ReadUInt16(r, &pk.Ack)
	if err != nil {
		return 0, err
	}

	// ack bits: 4 bytes
	err = ReadUInt32(r, &pk.AckBits)
	if err != nil {
		return 0, err
	}

	// message count 2 bytes
	err = ReadUInt16(r, &pk.MessageCount)
	if err != nil {
		return 0, err
	}

	if r.Len() == 0 {
		return 0, errors.New("bad packet length")
	}
	pk.Payload = data[30:]
	return len(data), nil
}

// IsValidChecksum checks validity of Checksum of OrcaPacket using the MagicID
func (pk *OrcaPacket) IsValidChecksum() bool {
	// the total packet is the MagicID + Payload
	tot := append(MagicID, pk.Payload...)
	cs := crc32.ChecksumIEEE(tot)
	return cs == pk.Checksum
}

// UnmarshalBody deserializes the sub-messages in the Packet payload
func (pk *OrcaPacket) UnmarshalBody() error {
	msgs := make([]*OrcaMessage, pk.MessageCount)
	rd := 0
	for i := uint16(0); i < pk.MessageCount; i++ {
		msgs[i] = &OrcaMessage{}
		n, err := msgs[i].UnmarshalBinary(pk.Payload[rd:])
		if err != nil {
			return err
		}
		rd += n
	}
	pk.Messages = msgs
	return nil
}

// NewOrcaMessage creates a new OrcaMessage wrapper
func NewOrcaMessage(tag uint16, body OrcaMarshaler) *OrcaMessage {
	om := OrcaMessage{
		Tag:  tag,
		ID:   0,
		Body: body,
	}
	return &om
}

// MarshalBinary writes the OrcaMessage struct to a buffer.
func (om *OrcaMessage) MarshalBinary() ([]byte, error) {
	buf := new(bytes.Buffer)
	err := WriteUInt16(buf, om.Tag)
	if err != nil {
		return nil, err
	}
	err = WriteUInt16(buf, om.ID)
	if err != nil {
		return nil, err
	}
	data, err := om.Body.MarshalBinary()
	if err != nil {
		return nil, err
	}
	buf.Write(data)
	return buf.Bytes(), err
}

// UnmarshalBinary reads binary data into an OrcaMessage
func (om *OrcaMessage) UnmarshalBinary(data []byte) (int, error) {
	return om.unmarshal(data, 0, false)
}

// UnmarshalIf unmarshals a message if it is the correct tag.
// Used in handshake.
func (om *OrcaMessage) UnmarshalIf(data []byte, tag uint16) error {
	_, err := om.unmarshal(data, tag, true)
	return err
}

func (om *OrcaMessage) unmarshal(data []byte, tg uint16, ck bool) (int, error) {
	r := bytes.NewReader(data)
	brd := 0
	err := ReadUInt16(r, &om.Tag)
	if err != nil {
		return 0, err
	}
	if ck && om.Tag != tg {
		return 0, errors.New("unexpected tag during unmarshal")
	}
	brd += 2
	err = ReadUInt16(r, &om.ID)
	if err != nil {
		return 0, err
	}
	brd += 2
	unpack, ok := unpackerMap[om.Tag]
	if !ok {
		// bad tag
		return 0, fmt.Errorf("Invalid tag:", om.Tag)
	}

	// unpack the message body (NOTE: could be empty)
	n, err := unpack(om, data[brd:])
	if err != nil {
		return 0, err
	}
	brd += n
	return brd, nil
}

func unpackHeartbeat(om *OrcaMessage, data []byte) (int, error) {
	// nothing to unpack in message body
	return 0, nil
}

func unpackJoined(om *OrcaMessage, data []byte) (int, error) {
	om.Body = new(WhaleJoined)
	return om.Body.UnmarshalBinary(data)
}

func unpackConfirmJoined(om *OrcaMessage, data []byte) (int, error) {
	om.Body = new(WhaleConfirmJoined)
	return om.Body.UnmarshalBinary(data)
}

func unpackReady(om *OrcaMessage, data []byte) (int, error) {
	om.Body = new(WhaleReady)
	return om.Body.UnmarshalBinary(data)
}

func unpackLeft(om *OrcaMessage, data []byte) (int, error) {
	om.Body = new(WhaleLeft)
	return om.Body.UnmarshalBinary(data)
}

func unpackTest(om *OrcaMessage, data []byte) (int, error) {
	om.Body = new(TestMessage)
	return om.Body.UnmarshalBinary(data)
}

func unpackStart(om *OrcaMessage, data []byte) (int, error) {
	om.Body = new(StartGame)
	return om.Body.UnmarshalBinary(data)
}

func unpackStop(om *OrcaMessage, data []byte) (int, error) {
	return 0, nil // nothing to unpack, just a signal really
}

func unpackFindMatch(om *OrcaMessage, data []byte) (int, error) {
	return 0, nil // nothing to unpack, signal
}

func unpackConnect(om *OrcaMessage, data []byte) (int, error) {
	return 0, nil
}

func unpackConnected(om *OrcaMessage, data []byte) (int, error) {
	om.Body = new(WhaleConnected)
	return om.Body.UnmarshalBinary(data)
}

func unpackConnReq(om *OrcaMessage, data []byte) (int, error) {
	om.Body = new(ConnectionRequest)
	return om.Body.UnmarshalBinary(data)
}

func unpackConnChall(om *OrcaMessage, data []byte) (int, error) {
	om.Body = new(ConnectionChallenge)
	return om.Body.UnmarshalBinary(data)
}

func unpackConnChallResp(om *OrcaMessage, data []byte) (int, error) {
	om.Body = new(ConnChallResp)
	return om.Body.UnmarshalBinary(data)
}

// helper for creating Magic
func genProtocol() []byte {
	h := fnv.New32()
	h.Write([]byte(Protocol))
	u := h.Sum32()
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.LittleEndian, u)
	return buf.Bytes()
}
