package messaging

import (
	"time"
)

const seqBufSize uint16 = 1024

// BufferData tracks info about a SequenceBuffer entry
type BufferData struct {
	Acked bool
	Data  interface{}
}

// MessageData tracks message info for sending
// TODO: consider moving this out...
type MessageData struct {
	Message  *OrcaMessage
	LastSent time.Time
	Raw      []byte
}

// OnAcked provides a callback for acking BufferData
type OnAcked func(pd *BufferData)

// SequenceBuffer is a rolling buffer for tracking acks
type SequenceBuffer struct {
	Buffer []uint16
	Data   []BufferData // NOTE: not a pointer
}

// NewSequenceBuffer creates a new SequenceBuffer
func NewSequenceBuffer() *SequenceBuffer {
	return &SequenceBuffer{
		Buffer: make([]uint16, seqBufSize, seqBufSize),
		Data:   make([]BufferData, seqBufSize, seqBufSize),
	}
}

// SetBufferData overwrites BufferData for a Packet sequence number
// Use []uint16 for PacketData's MessageIDs
// Use *OrcaMessage for MessageData's message 1 0 nil true
func (sb *SequenceBuffer) SetBufferData(seq, prev uint16, data interface{}, recvPk bool) {
	i := seq % seqBufSize     // TODO: should this be -1?
	sb.Buffer[i] = seq        // update this entry
	sb.Data[i].Acked = recvPk // if we received a packet, mark acked, so we know to send in ackBits
	sb.Data[i].Data = data    // set to nil when received, as we don't care. other side will track through PacketData

	// on receiving a packet: must clear entries between previous insert and latest
	// to prevent false acks on uint rollaround
	// NOTE: NOT uint rollaround, buffer size rollaround! either do the mod each time or go buf size
	// TODO: consider breaking out
	if recvPk {
		ii := prev % seqBufSize // this is the buffer index! not the seq
		if ii < i {
			sb.clearBufferData(ii+1, i) // walk between and mark unacked
		} else {
			sb.clearBufferData(ii, seqBufSize) // walk prev to end
			sb.clearBufferData(0, i)           // walk beginning up to current
		}
	}
}

// GetBufferData returns a pointer to BufferData for a sequence number
func (sb *SequenceBuffer) GetBufferData(seq uint16) *BufferData {
	i := seq % seqBufSize
	if sb.Buffer[i] == seq {
		return &sb.Data[i]
	}
	return nil
}

// GetAckBits gets the ack bits for
func (sb *SequenceBuffer) GetAckBits(seq uint16) uint32 {
	var acked uint32
	// NOTE: optimize indexing if desired
	for i := uint32(0); i < 32; i++ {
		s := seq - uint16(i)
		d := sb.GetBufferData(s)
		if d != nil && d.Acked {
			acked |= (1 << i)
		}
	}
	return acked
}

// Ack decodes and marks as acked
func (sb *SequenceBuffer) Ack(ack uint16, ackBits uint32, onAcked OnAcked) {
	for i := uint32(0); i < 32; i++ {
		b := uint32(1) << i
		if (ackBits & b) != 0 {
			seq := ack - uint16(i)
			bd := sb.GetBufferData(seq)
			if bd != nil && !bd.Acked {
				bd.Acked = true
				onAcked(bd)
			}
		}
	}
}

// clears data between indices
func (sb *SequenceBuffer) clearBufferData(start, end uint16) {
	for i := start; i < end; i++ {
		sb.Buffer[i] = 0
		sb.Data[i].Acked = false
	}
}
