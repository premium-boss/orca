package messaging

import (
	"bytes"
	"encoding/binary"
)

var end binary.ByteOrder = binary.LittleEndian

// WriteUInt8 writes a uint8 to a buffer
func WriteUInt8(buf *bytes.Buffer, u uint8) error {
	return binary.Write(buf, end, u)
}

// WriteInt16 writes an int16 to a buffer
func WriteInt16(buf *bytes.Buffer, i int16) error {
	return binary.Write(buf, end, i)
}

// WriteUInt16 writes a uint16 to a buffer
func WriteUInt16(buf *bytes.Buffer, u uint16) error {
	return binary.Write(buf, end, u)
}

// WriteInt32 writes an int32 to a buffer
func WriteInt32(buf *bytes.Buffer, i int32) error {
	return binary.Write(buf, end, i)
}

// WriteUInt32 writes a uint32 to a buffer
func WriteUInt32(buf *bytes.Buffer, u uint32) error {
	return binary.Write(buf, end, u)
}

// WriteInt64 writes an int64 to a buffer
func WriteInt64(buf *bytes.Buffer, i int64) error {
	return binary.Write(buf, end, i)
}

// WriteUInt64 writes a uint64 to a buffer
func WriteUInt64(buf *bytes.Buffer, u uint64) error {
	return binary.Write(buf, end, u)
}

// WriteFloat32 writes a float32 to a buffer
func WriteFloat32(buf *bytes.Buffer, f float32) error {
	return binary.Write(buf, end, f)
}

// WriteFloat64 writes a float64 to a buffer
func WriteFloat64(buf *bytes.Buffer, f float64) error {
	return binary.Write(buf, end, f)
}

// WritePrefixedString writes a string to a buffer, first w/ length prefix
func WritePrefixedString(buf *bytes.Buffer, s string) error {
	sbuf := new(bytes.Buffer)
	// TODO: check encoding here.. may need to enforce
	n, err := sbuf.WriteString(s)
	if err == nil {
		err = WriteInt32(buf, int32(n))
	}
	if err == nil {
		_, err = buf.Write(sbuf.Bytes())
	}
	return err
}

// ReadUInt8 reads a uint8 from the byte array
func ReadUInt8(rd *bytes.Reader, u *uint8) error {
	return binary.Read(rd, end, u)
}

// ReadInt16 reads an int16 from the byte array
func ReadInt16(rd *bytes.Reader, i *int16) error {
	return binary.Read(rd, end, i)
}

// ReadUInt16 reads a uint16 from the byte array
func ReadUInt16(rd *bytes.Reader, u *uint16) error {
	return binary.Read(rd, end, u)
}

// ReadInt32 reads an int32 from the byte array
func ReadInt32(rd *bytes.Reader, i *int32) error {
	return binary.Read(rd, end, i)
}

// ReadUInt32 reads a uint32 from the byte array
func ReadUInt32(rd *bytes.Reader, u *uint32) error {
	return binary.Read(rd, end, u)
}

// ReadInt64 reads an int64 from the byte array
func ReadInt64(rd *bytes.Reader, i *int64) error {
	return binary.Read(rd, end, i)
}

// ReadUInt64 reads a uint64 from the byte array
func ReadUInt64(rd *bytes.Reader, u *uint64) error {
	return binary.Read(rd, end, u)
}

// ReadFloat32 reads a float32 from the byte array
func ReadFloat32(rd *bytes.Reader, f *float32) error {
	return binary.Read(rd, end, f)
}

// ReadFloat64 reads a float64 from the byte array
func ReadFloat64(rd *bytes.Reader, f *float64) error {
	return binary.Read(rd, end, f)
}

// ReadPrefixedString reads a string from the byte array
func ReadPrefixedString(rd *bytes.Reader) (int, string, error) {
	var n int32
	err := ReadInt32(rd, &n)
	s := ""
	if err == nil {
		buf := make([]byte, n)
		_, err := rd.Read(buf)
		if err == nil {
			s = string(buf)
		}
	}
	if err != nil {
		return 0, "", err
	}
	return int(n) + 4, s, nil
}
