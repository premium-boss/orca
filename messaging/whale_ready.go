package messaging

import "bytes"

// WhaleReady is a player ready message
type WhaleReady struct {
	ID uint16
}

// MarshalBinary writes a WhaleReady message to bytes
func (wr *WhaleReady) MarshalBinary() ([]byte, error) {
	buf := new(bytes.Buffer)
	err := WriteUInt16(buf, wr.ID)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// UnmarshalBinary reads a WhaleReady message from data
func (wr *WhaleReady) UnmarshalBinary(data []byte) (int, error) {
	rd := bytes.NewReader(data)
	err := ReadUInt16(rd, &wr.ID)
	if err != nil {
		return 0, err
	}
	return 2, nil
}
