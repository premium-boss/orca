package messaging

// StopGame is a simple directive
type StopGame struct{}

// MarshalBinary writes to bytes
func (sg *StopGame) MarshalBinary() ([]byte, error) {
	b := make([]byte, 0)
	return b, nil
}

// UnmarshalBinary reads data from bytes
func (sg *StopGame) UnmarshalBinary(data []byte) (int, error) {
	return 0, nil
}
