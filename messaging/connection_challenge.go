package messaging

import (
	"bytes"
)

// ConnectionChallenge is a message from server to client during handshake.
type ConnectionChallenge struct {
	ClientSalt int64
	ServerSalt int64
}

// MarshalBinary serializes ConnectionChallenge message.
func (cc *ConnectionChallenge) MarshalBinary() ([]byte, error) {
	buf := new(bytes.Buffer)
	err := WriteInt64(buf, cc.ClientSalt)
	if err != nil {
		return nil, err
	}
	err = WriteInt64(buf, cc.ServerSalt)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// UnmarshalBinary deserializes ConnectionChallenge message.
func (cc *ConnectionChallenge) UnmarshalBinary(data []byte) (int, error) {
	rd := bytes.NewReader(data)
	err := ReadInt64(rd, &cc.ClientSalt)
	if err != nil {
		return 0, err
	}
	err = ReadInt64(rd, &cc.ServerSalt)
	if err != nil {
		return 0, err
	}
	return 16, nil
}
