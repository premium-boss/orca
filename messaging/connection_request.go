package messaging

import (
	"bytes"
)

// ConnectionRequest is a client connection request message.
type ConnectionRequest struct {
	ClientSalt int64
}

// MarshalBinary serilializes the message.
func (cr *ConnectionRequest) MarshalBinary() ([]byte, error) {
	buf := new(bytes.Buffer)
	err := WriteInt64(buf, cr.ClientSalt)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// UnmarshalBinary deserilializes the message.
func (cr *ConnectionRequest) UnmarshalBinary(data []byte) (int, error) {
	rd := bytes.NewReader(data)
	err := ReadInt64(rd, &cr.ClientSalt)
	if err != nil {
		return 0, err
	}
	return 8, nil
}
