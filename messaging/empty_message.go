package messaging

// EmptyMessage is just a blank body for
// messages that only require a header.
type EmptyMessage struct {
}

// MarshalBinary serialilzes message
func (em *EmptyMessage) MarshalBinary() ([]byte, error) {
	return make([]byte, 0, 0), nil
}

// UnmarshalBinary deserializes message
func (em *EmptyMessage) UnmarshalBinary(data []byte) (int, error) {
	return 0, nil
}
