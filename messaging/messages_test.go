package messaging

import "testing"

func TestOrcaMessage(t *testing.T) {
	// using a test message, we'll test that serializing and
	// deserializing work properly
	tm := TestMessage{
		Num:        17,
		Coordinate: -22.8907777908,
		Message:    "Hello, world!!!???!!! I am Monkey D. Luffy,@@@@%%^^^^^^^YUPPP",
	}
	om := NewOrcaMessage(TagTest, &tm)
	data, err := om.MarshalBinary()
	if err != nil {
		t.Fatal("Error marshaling Orca message", err)
	}
	if len(data) == 0 {
		t.Fatal("Error: length of serialized Orca message was 0")
	}
	recv := new(OrcaMessage)
	_, err = recv.UnmarshalBinary(data)
	if err != nil {
		t.Fatal("Error unmarshaling Orca message", err)
	}
	recvtm := recv.Body.(*TestMessage)
	if recvtm.Num != tm.Num {
		t.Fatalf("Actual Num %d does not match expected %d", recvtm.Num, tm.Num)
	}
	if recvtm.Coordinate != tm.Coordinate {
		t.Fatalf("Actual Coordinate %f does not match expected %f",
			recvtm.Coordinate, tm.Coordinate)
	}
	if recvtm.Message != tm.Message {
		t.Fatalf("Actual Message %s does not match expected %s", recvtm.Message, tm.Message)
	}
}
