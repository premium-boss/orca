package messaging

import (
	"fmt"
	"time"
)

// BuildAddr combines an ip and port into an address string
func BuildAddr(ip string, port int) string {
	return fmt.Sprintf("%s:%d", ip, port)
}

// ResetTimer resets a timer to a duration
func ResetTimer(t *time.Timer, d *time.Duration) {
	if !t.Stop() {
		<-t.C // flush
	}
	t.Reset(*d)
}
