package messaging

import "bytes"

// WhaleLeft is a player disconnect / quit message
type WhaleLeft struct {
	ID uint16
}

// MarshalBinary writes a WhaleLeft message to bytes
func (wl *WhaleLeft) MarshalBinary() ([]byte, error) {
	buf := new(bytes.Buffer)
	err := WriteUInt16(buf, wl.ID)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// UnmarshalBinary reads a WhaleLeft message from data
func (wl *WhaleLeft) UnmarshalBinary(data []byte) (int, error) {
	rd := bytes.NewReader(data)
	err := ReadUInt16(rd, &wl.ID)
	if err != nil {
		return 0, err
	}
	return 2, nil
}
