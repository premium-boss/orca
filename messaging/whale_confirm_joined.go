package messaging

import "bytes"

// WhaleConfirmJoined is a player joined message
type WhaleConfirmJoined struct {
	ID uint16
}

// MarshalBinary writes a WhaleConfirmJoined message to bytes
func (wj *WhaleConfirmJoined) MarshalBinary() ([]byte, error) {
	buf := new(bytes.Buffer)
	err := WriteUInt16(buf, wj.ID)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// UnmarshalBinary reads a WhaleConfirmJoined message from data
func (wj *WhaleConfirmJoined) UnmarshalBinary(data []byte) (int, error) {
	rd := bytes.NewReader(data)
	err := ReadUInt16(rd, &wj.ID)
	if err != nil {
		return 0, err
	}
	return 2, nil
}
