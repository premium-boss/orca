package messaging

import (
	"bytes"
)

// StartGame is a simple directive
type StartGame struct {
	UnixTime  int64
	PlayerIDs uint8
}

// MarshalBinary writes data to bytes
func (sg *StartGame) MarshalBinary() ([]byte, error) {
	buf := new(bytes.Buffer)
	if err := WriteInt64(buf, sg.UnixTime); err != nil {
		return nil, err
	}
	if err := WriteUInt8(buf, sg.PlayerIDs); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// UnmarshalBinary reads from data
func (sg *StartGame) UnmarshalBinary(data []byte) (int, error) {
	rd := bytes.NewReader(data)
	if err := ReadInt64(rd, &sg.UnixTime); err != nil {
		return 0, err
	}
	if err := ReadUInt8(rd, &sg.PlayerIDs); err != nil {
		return 0, err
	}
	return 9, nil
}

// EncodeIDs ...
func (sg *StartGame) EncodeIDs(ids []int) {
	// TODO: check cap... this only enables 8 players right now...make a dynamic array as needed..., or multiple w/ multipliers...
	a := uint8(0)
	for _, id := range ids {
		a |= (1 << uint8(id))
	}
	sg.PlayerIDs = a
}

// DecodeIDs ...
func (sg *StartGame) DecodeIDs() []int {
	ids := make([]int, 0, 8)
	for i := uint8(0); i < 8; i++ {
		b := uint8(1) << i
		if sg.PlayerIDs&b != 0 {
			ids = append(ids, int(i))
		}
	}
	return ids
}
