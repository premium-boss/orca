package messaging

import "bytes"

// TestMessage is a simple struct for testing
type TestMessage struct {
	Num        int32
	Coordinate float64
	Message    string
}

// MarshalBinary turns a test message into binary
func (tm *TestMessage) MarshalBinary() ([]byte, error) {
	buf := new(bytes.Buffer)
	err := WriteInt32(buf, tm.Num)
	if err == nil {
		err = WriteFloat64(buf, tm.Coordinate)
	}
	if err == nil {
		err = WritePrefixedString(buf, tm.Message)
	}
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// UnmarshalBinary reads a TestMessage out of binary
func (tm *TestMessage) UnmarshalBinary(data []byte) (int, error) {
	r := bytes.NewReader(data)
	err := ReadInt32(r, &tm.Num)
	if err == nil {
		err = ReadFloat64(r, &tm.Coordinate)
	}
	var sn int
	if err == nil {
		sn, tm.Message, err = ReadPrefixedString(r)
	}
	if err != nil {
		return 0, err
	}
	return 12 + sn, nil
}
