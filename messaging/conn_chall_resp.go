package messaging

import (
	"bytes"
)

// ConnChallResp is a cilent response message to a ConnectionChallenge.
type ConnChallResp struct {
	ClientSalt int64
	ServerSalt int64
}

// MarshalBinary serializes the message.
func (ccr *ConnChallResp) MarshalBinary() ([]byte, error) {
	buf := new(bytes.Buffer)
	err := WriteInt64(buf, ccr.ClientSalt)
	if err != nil {
		return nil, err
	}
	err = WriteInt64(buf, ccr.ServerSalt)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// UnmarshalBinary deserializes the message.
func (ccr *ConnChallResp) UnmarshalBinary(data []byte) (int, error) {
	rd := bytes.NewReader(data)
	err := ReadInt64(rd, &ccr.ClientSalt)
	if err != nil {
		return 0, err
	}
	err = ReadInt64(rd, &ccr.ServerSalt)
	return 16, nil
}
