package messaging

import (
	"bytes"
	"log"
	"time"

	"github.com/google/uuid"
)

// Packet and Message constants
const (
	ClientMessageBufferSize uint16 = 250  // indicates the max amount of messages a client will buffer
	OrcaConnMaxPayloadSize  int    = 1442 // 1500 mtu - 20 IP hdr - 8 UDP hdr = 1472 - 30 packet hdr = 1442, TODO: adjust for signing...
)

// MessageHandler handles a message
type MessageHandler func(*OrcaMessage)

// PacketTracker is a struct for managing packets, messages, and acks
type PacketTracker struct {
	TxSequence         uint16          // current running sequence for receiving Packets
	TxMessageID        uint16          // running sequence for receiving Messages
	TxUnackedMessageID uint16          // earliest unacked message ID, for resending
	TxPacketBuffer     *SequenceBuffer // Tracks outgoing Packets so we know what messages were received upon Ack
	TxMessageBuffer    *SequenceBuffer // Tracks outgoing Messages so we know what to resend in next packet
	RxSequence         uint16          // current running sequence for sending Packets
	RxMessageID        uint16          // current running sequence for sending Messages
	RxPacketBuffer     *SequenceBuffer // Tracks incoming Packets so we know what to apply and Ack
	RxMessageBuffer    *SequenceBuffer // Tracks incoming Messages so we can apply them in order
}

// NewPacketTracker creates and initializes a new PacketTracker
func NewPacketTracker() PacketTracker {
	return PacketTracker{
		TxSequence:         1, // must start at 1
		TxMessageID:        0,
		TxUnackedMessageID: 0,
		TxPacketBuffer:     NewSequenceBuffer(),
		TxMessageBuffer:    NewSequenceBuffer(),
		RxMessageID:        0,
		RxSequence:         0,
		RxPacketBuffer:     NewSequenceBuffer(),
		RxMessageBuffer:    NewSequenceBuffer(),
	}
}

// QueueMessage sets up ID and queues in SequenceBuffer
// so we can send this message.
func (pt *PacketTracker) QueueMessage(msg *OrcaMessage) {
	msg.ID = pt.TxMessageID
	pt.TxMessageID++
	md := MessageData{Message: msg, LastSent: time.Time{}}
	pt.TxMessageBuffer.SetBufferData(msg.ID, 0, &md, false)
}

// GetNextMessages return the next messages and ids to send in a packet.
// We walk between the last unacked message ID and the latest ID.
// We can't add anything bigger than last unacked + the client's buffer size.
// We can't add anything that doesn't fit in packet.
// We shouldn't add anything that was sent in the last 0.1 sec.
// Mark the send time of those we're sending.
// NOTE: take care w/ uint16 rollaround
func (pt *PacketTracker) GetNextMessages() ([]uint16, []byte) {
	ids := make([]uint16, 0, ClientMessageBufferSize)
	mx := pt.TxUnackedMessageID + ClientMessageBufferSize
	st := pt.TxMessageID                                              // this is already 1 beyond the last queued
	t := time.Now().UTC().Add(-time.Duration(100) * time.Millisecond) // go back 100 ms
	sz := 0
	buf := new(bytes.Buffer)
	for i := pt.TxUnackedMessageID; i != mx && i != st; i++ {
		bd := pt.TxMessageBuffer.GetBufferData(i)
		if bd.Acked {
			continue // already received
		}
		md := bd.Data.(*MessageData)
		if md.LastSent.After(t) {
			continue // sent too recently
		}
		if md.Raw == nil {
			data, err := md.Message.MarshalBinary()
			if err != nil {
				break // TODO: log, remove?
			}
			md.Raw = data // cache the serialized data
		}
		n := len(md.Raw)
		if sz+n > OrcaConnMaxPayloadSize {
			break // payload too big
		}
		sz += n
		buf.Write(md.Raw)
		md.LastSent = time.Now().UTC()
		ids = append(ids, md.Message.ID)
	}
	return ids, buf.Bytes()
}

// updateLastTxUnacked walks from the last unacked sent message ID
// and stops at the first unacked message or the most recent send ID,
// whichever comes first.
// NOTE: take care to deal w/ uint16 rollaround
func (pt *PacketTracker) updateLastTxUnacked() {
	i := pt.TxUnackedMessageID
	for i != pt.TxMessageID {
		bd := pt.TxMessageBuffer.GetBufferData(i)
		if bd != nil && !bd.Acked {
			break
		}
		i++
	}
	pt.TxUnackedMessageID = i
}

// CreateNextPacket reads from message buffer and creates the next tx packet
func (pt *PacketTracker) CreateNextPacket(sessionID uuid.UUID) (bool, []byte, error) {
	ack := pt.RxSequence
	ackBits := pt.RxPacketBuffer.GetAckBits(ack)
	ids, pd := pt.GetNextMessages()
	ct := uint16(len(ids))
	if ct == 0 {
		return false, nil, nil // nothing to send
	}
	//log.Printf("CREATING packet:: pk seq: %d, ct: %d, ack: %d, ack_bits: %b\n", pt.TxSequence, ct, ack, ackBits)
	pk := NewOrcaPacket(sessionID, pt.TxSequence, ack, ct, ackBits, pd)
	pt.TxPacketBuffer.SetBufferData(pk.Sequence, 0, ids, false)
	b, err := pk.MarshalBinary()
	if err != nil {
		return false, nil, err
	}
	pt.TxSequence++ // increment sequence
	return true, b, nil
}

// onSendPacketAcked handles the acking of a sent packet.
// Removes messages from send queue that were in this packet.
func (pt *PacketTracker) onSendPacketAcked(bd *BufferData) {
	ids := bd.Data.([]uint16) // the data for a packet is a slice of message IDs
	for _, id := range ids {
		bd := pt.TxMessageBuffer.GetBufferData(id)
		if bd != nil {
			bd.Acked = true
			bd.Data = nil // remove msg ref to allow GC
		}
	}
}

// ReceivePacket validates and applies a packet and messages
// in the order intented. Messages are buffered until the
// next in line is received.
func (pt *PacketTracker) ReceivePacket(pk *OrcaPacket) []*OrcaMessage {
	// check Packet sequence (take care for uint)
	diff := pk.Sequence - pt.RxSequence
	valid := diff > 0 && diff < 1000 // uint16s: can't be same seq, can't be behind, can't be too far ahead (behind by rollaround)
	if !valid {
		log.Println("not valid packet, bad sequence, expected:", pt.RxSequence, "got:", pk.Sequence)
		return nil
	}
	pt.RxPacketBuffer.SetBufferData(pk.Sequence, pt.RxSequence, nil, true)
	pt.RxSequence = pk.Sequence

	// handle our messages that are now acked
	// NOTE: onSendPacketAcked needs to mark the messages as acked BEFORE updating last acked
	pt.TxPacketBuffer.Ack(pk.Ack, pk.AckBits, pt.onSendPacketAcked)

	// Update the last unacked message ID
	pt.updateLastTxUnacked()

	// add messages to message receive buffer
	for i := uint16(0); i < pk.MessageCount; i++ {
		msg := pk.Messages[i]
		if msg.ID >= pt.RxMessageID {
			// set to seq buffer, can't have been handled yet
			pt.RxMessageBuffer.SetBufferData(msg.ID, 0, msg, false)
		}
	}
	pk.Messages = nil

	var msgs []*OrcaMessage
	for {
		bd := pt.RxMessageBuffer.GetBufferData(pt.RxMessageID)
		if bd == nil {
			break // we haven't received it yet
		}
		msg, ok := bd.Data.(*OrcaMessage)
		// NOTE: need extra check, as num will match on initial 0 ...
		if !ok {
			break
		}
		pt.RxMessageID++
		msgs = append(msgs, msg)
		bd.Data = nil // clear it out
	}
	return msgs // return the messages to handle now
}
