package main

import (
	gm "gitlab.com/premium-boss/orca/game"
	om "gitlab.com/premium-boss/orca/messaging"
)

// EmptyGame is a non-functioning client Game implementation.
// Used to test the client handshake with this CLI form.
type EmptyGame struct {
}

// Init ...
func (eg *EmptyGame) Init(ch chan<- gm.GameMessage) {

}

// Start ...
func (eg *EmptyGame) Start(ids []int) {

}

// Stop ...
func (eg *EmptyGame) Stop() {

}

// SetPlayer ...
func (eg *EmptyGame) SetPlayer(ID uint16) {

}

// HandleMessage ...
func (eg *EmptyGame) HandleMessage(msg *om.OrcaMessage) {

}

// Pause ...
func (eg *EmptyGame) Pause() {

}

// Unpause ...
func (eg *EmptyGame) Unpause() {

}
