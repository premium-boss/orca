package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"

	"gitlab.com/premium-boss/orca/client"
)

const (
	defaultServerPort int    = 5050
	defaultClientPort int    = 7070
	defaultIP         string = "127.0.0.1"
)

func main() {
	ip := flag.String("ip", defaultIP, "IP address of server")
	cport := flag.Int("rx", defaultClientPort, "port for client to receive msgs from server")
	sport := flag.Int("tx", defaultServerPort, "port to which client will send server msgs")
	v := flag.Bool("v", false, "verbose logging")
	f := flag.Bool("f", false, "use log file")
	flag.Parse()
	fmt.Println("starting ORCA client...")
	rand.Seed(time.Now().UnixNano())
	opts := client.LogOpts{UseFile: *f, Verbose: *v}
	gm := EmptyGame{}
	c, err := client.NewClient(opts, &gm)
	if err != nil {
		log.Println("could not start client", err)
		return
	}
	err = c.Connect(*ip, *cport, *sport)
	if err != nil {
		log.Println("could not connect", err)
		return
	}
	defer c.Close()
	go c.Run()

	// keep running until quit
	done := make(chan bool, 1)
	printCommands()
	go readCommand(c, done)
	<-done
	fmt.Println("client closing. Goodbye!")
}

// TODO: clean up
func readCommand(c *client.Client, done chan<- bool) {
Loop:
	for {
		reader := bufio.NewReader(os.Stdin)
		text, _ := reader.ReadString('\n')
		text = strings.Trim(text, "\n")
		// TODO: be able to parse pattern, flags, e.g.
		switch text {
		case "join":
			c.Join()
		case "ready":
			c.Ready()
		case "leave":
			c.Leave()
			// TODO: leave without disconnecting...
		case "quit":
			// TODO: send something to client?
			break Loop
		}
		printCommands()
	}
	done <- true
}

func printCommands() {
	cmds := []string{"'join'", "'ready'", "'leave'", "'input'", "'quit'"}
	s := ""
	for _, c := range cmds {
		s += c + "\n"
	}
	fmt.Printf("commands are: \n%s\n", s)
}
