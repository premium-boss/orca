package client

import gm "gitlab.com/premium-boss/orca/game"

// Game is a game interface specifically for the Client
type Game interface {
	gm.Game
	SetPlayer(id uint16)
}
