package client

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"os"
	"time"

	gm "gitlab.com/premium-boss/orca/game"
	om "gitlab.com/premium-boss/orca/messaging"
	sig "gitlab.com/premium-boss/orca/signal"

	"github.com/google/uuid"
)

const (
	heartbeatSeconds        int    = 5
	timeoutSeconds          int    = 10 // allow a little more time
	connectTries            int    = 5
	logNameDate             string = "20060102_150405"
	connectionRequestLength int    = 1024 // padded length of connection req
)

// Client is a game client that connects to Orca
type Client struct {
	Conn       *net.UDPConn
	RemoteAddr *net.UDPAddr
	PkTrk      om.PacketTracker
	SessionID  uuid.UUID
	PlayerID   uint16
	PkChan     chan *om.OrcaPacket
	SigChan    chan *sig.Signal
	IsRunning  bool
	GameChan   chan gm.GameMessage
	Game       Game
	logger     *log.Logger
	clientSalt int64
	serverSalt int64
}

// LogOpts are options for Client logging. Extend as needed.
type LogOpts struct {
	Verbose bool
	UseFile bool
}

// NewClient creates a new client
func NewClient(opts LogOpts, game Game) (*Client, error) {
	c := Client{
		SessionID: om.BlankID,
		PkChan:    make(chan *om.OrcaPacket, 100),
		SigChan:   make(chan *sig.Signal, 16),
		PkTrk:     om.NewPacketTracker(),
		GameChan:  make(chan gm.GameMessage, 1256),
		Game:      game,
	}

	err := c.initLog(opts)
	if err != nil {
		return nil, err
	}
	c.Game.Init(c.GameChan)
	return &c, nil
}

// Connect connects to the server: TODO: handshake
func (c *Client) Connect(ip string, clientPort int, serverPort int) error {
	addr := om.BuildAddr(ip, serverPort)
	udpAddr, err := net.ResolveUDPAddr("udp", addr)
	clientAddr := om.BuildAddr("", clientPort)
	localAddr, err := net.ResolveUDPAddr("udp", clientAddr)
	if err != nil {
		return err
	}
	c.RemoteAddr = udpAddr
	c.logger.Printf("connecting to %s:%d\n", ip, serverPort)

	c.Conn, err = net.DialUDP("udp", localAddr, c.RemoteAddr)
	if err != nil {
		c.logger.Println("could not dial UDP")
		return err
	}
	ok := false
	for try := 0; try < connectTries; try++ {
		ok, err = c.handshake() // 5 tries total
		if err != nil {
			c.logger.Println("error with handshake:", err)
			time.Sleep(time.Duration(5) * time.Second) // sleep a bit, try again
		}
		if ok {
			break // we did it!
		}
		// start a new attempt: if err was nil, we timed out last time
	}
	if !ok {
		c.logger.Println("unable to complete handshake")
		return errors.New("unable to complete handshake: out of attempts")
	}
	return nil
}

// Run runs the client loop
func (c *Client) Run() {
	c.IsRunning = true
	go c.listen() // TODO: kill chan
	heartTick := time.NewTicker(time.Duration(heartbeatSeconds) * time.Second)
	sendTick := time.NewTicker(time.Millisecond * time.Duration(50)) // message rate, not game!
	timeoutDur := time.Duration(timeoutSeconds) * time.Second
	timeout := time.NewTimer(timeoutDur)
	c.queueHeartbeat() // queue up a first heartbeat so we send right away
	isRunning := true
	for isRunning {
		select {
		case <-heartTick.C:
			c.queueHeartbeat()
		case <-timeout.C:
			c.logger.Println("timeout from server")
			isRunning = false // TODO: kill and close gracefully
		case <-sendTick.C:
			c.send() // TODO: get error: consider handling...
		case s := <-c.SigChan:
			c.handleSignal(s)
		case pk := <-c.PkChan:
			om.ResetTimer(timeout, &timeoutDur)
			msgs := c.PkTrk.ReceivePacket(pk) // NOTE: this is an already validated packet
			for _, m := range msgs {
				c.handleMessage(m)
			}
		case msg := <-c.GameChan:
			// message from Game. We could check for control messages, but for
			// now, assume input of some sort that needs to go to server
			// HACK: due to resuse of Game interface for server and client
			c.PkTrk.QueueMessage(msg.Message)
		}

	}
	// TODO: kill and close gracefully
}

// Join queues up a join message.
// TODO: isconnected flag, or ClientState machine
func (c *Client) Join() {
	msg := om.NewOrcaMessage(om.TagFindMatch, &om.EmptyMessage{})
	c.sendMessage(msg)
}

// Ready sends a ready message.
// TODO: isJoined flag, or ClientState machine
func (c *Client) Ready() {
	msg := om.NewOrcaMessage(om.TagReady, &om.WhaleReady{ID: c.PlayerID})
	c.sendMessage(msg)
}

// Leave sends a leave message
// TODO: check state
func (c *Client) Leave() {
	msg := om.NewOrcaMessage(om.TagLeft, &om.WhaleLeft{ID: c.PlayerID})
	c.sendMessage(msg)
}

// Stop stops the Client
func (c *Client) Stop() {
	s := sig.Kill()
	c.SigChan <- &s
}

// Close closes underlying connection
func (c *Client) Close() {
	if c.Conn != nil {
		c.Conn.Close()
	}
}

// sends message in signal to Client run loop
func (c *Client) sendMessage(msg *om.OrcaMessage) {
	s := sig.Signal{Sig: sig.SigSend, Data: msg}
	c.SigChan <- &s
}

// send sends next packet
func (c *Client) send() error {
	send, b, err := c.PkTrk.CreateNextPacket(c.SessionID)
	if err != nil {
		c.logger.Println("error creating packet", err)
		return err
	}
	if !send {
		return nil
	}
	_, err = c.Conn.Write(b) // NOTE: already connected, do NOT use WriteTo
	if err != nil {
		c.logger.Println("error sending packet", err)
	}
	return err
}

// handshake makes sure we get a SessionID before we start. It has 5 seconds
// to complete the handshake protocol, resending connection reqs and challenges
// within.
func (c *Client) handshake() (bool, error) {
	c.clientSalt = rand.Int63() // create salt
	c.logger.Println("attempting handshake with salt", c.clientSalt)
	c.serverSalt = 0 // reset server salt (not really necessary)
	fail := time.Now().Add(time.Duration(5) * time.Second)

	// send connection request and receive challenge
	for {
		ok, err := c.connectionRequest()
		if err != nil {
			return false, err
		}
		if ok {
			break // we got it!
		}
		if time.Now().After(fail) {
			return false, nil // timeout
		}
	}

	// send challenge response and receive session ID
	for {
		ok, err := c.connectionChallenge()
		if err != nil {
			return false, err
		}
		if ok {
			// we did it and we're connected!
			break
		}
		if time.Now().After(fail) {
			return false, nil // timeout
		}
	}
	return true, nil // success!
}

func (c *Client) connectionRequest() (bool, error) {
	// send a connect message
	cm := om.ConnectionRequest{ClientSalt: c.clientSalt}
	cmsg := om.NewOrcaMessage(om.TagConnectionRequest, &cm)
	if err := c.sendHandshakePacket(cmsg); err != nil {
		return false, err
	}
	pk, ok, err := c.receiveHandshakePacket()
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil // timed out on recv, try again
	}

	// check it's the challenge
	msg := om.OrcaMessage{}
	err = msg.UnmarshalIf(pk.Payload, om.TagConnectionChallenge)
	if err != nil {
		c.logger.Println("bad connection challenge")
		return false, err
	}
	cc := msg.Body.(*om.ConnectionChallenge)
	if cc.ClientSalt != c.clientSalt {
		c.logger.Println("bad client salt:", cc.ClientSalt, "expected:", c.clientSalt)
		return false, errors.New("bad client salt")
	}
	if cc.ServerSalt == 0 {
		return false, errors.New("bad server salt")
	}
	// we got a challenge! set server salt
	c.serverSalt = cc.ServerSalt
	return true, nil
}

func (c *Client) connectionChallenge() (bool, error) {
	chal := om.ConnectionChallenge{
		ClientSalt: c.clientSalt,
		ServerSalt: c.serverSalt,
	}
	cmsg := om.NewOrcaMessage(om.TagConnectionChallengeResponse, &chal)
	if err := c.sendHandshakePacket(cmsg); err != nil {
		return false, err
	}
	pk, ok, err := c.receiveHandshakePacket()
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil // timed out on recv, try again
	}
	err = pk.UnmarshalBody() // TODO: consider removing from unpacker map...
	msg := pk.Messages[0]
	if msg.Tag != om.TagConnected {
		return false, nil // could be an earlier or later packet, depending...
	}

	// we got connected!
	wc := msg.Body.(*om.WhaleConnected)
	c.logger.Println("recv TagConnected, SessionID:", wc.SessionID.String())
	c.SessionID = wc.SessionID

	// now can return true and start main processing loop!
	return true, nil
}

// sendHandshakePacket sends a one-time handshake packet outside of normal
// packet tracking and ack system. It pads to required length.
func (c *Client) sendHandshakePacket(msg *om.OrcaMessage) error {
	c.logger.Println("sending handshake packet")
	payload, err := msg.MarshalBinary()
	if err != nil {
		return err
	}
	padded := make([]byte, connectionRequestLength, connectionRequestLength) // TODO:! need to sub header... or check >
	copy(padded, payload)
	pk := om.NewOrcaPacket(om.BlankID, 0, 0, 1, 0, padded)
	data, err := pk.MarshalBinary()
	if err != nil {
		return err
	}
	_, err = c.Conn.Write(data)
	if err != nil {
		return err
	}
	return nil
}

// receiveHandshakePacket receives a single packet for handshake protocol
// outside of packet tracking or ack system, or else times out.
func (c *Client) receiveHandshakePacket() (*om.OrcaPacket, bool, error) {
	// listen for response:
	deadline := time.Now().Add(time.Duration(200) * time.Millisecond) // TODO: constant
	c.Conn.SetReadDeadline(deadline)
	data, err := c.listenOnce()

	// check if we errored, other than our set timeout
	if err != nil {
		nerr, ok := err.(net.Error)
		if ok && nerr.Timeout() {
			return nil, false, nil // this is okay, intentional timeout
		}
		return nil, false, err
	}

	// we got a message
	c.logger.Println("handhsake: read a message")

	// unmarshal packet w/ just header
	pk := om.OrcaPacket{}
	if _, err := pk.UnmarshalBinary(data); err != nil {
		log.Println("bad packet unmarshal: ", err)
		return nil, false, err // invalid overall packet
	}

	// validate
	if !pk.IsValidChecksum() {
		log.Println("bad checksum")
		return nil, false, errors.New("bad checksum")
	}

	// check message count
	if pk.MessageCount > 1 {
		return nil, false, errors.New("bad packet msg count")
	}
	return &pk, true, nil
}

// listen listens for incoming messages from the server...
// TODO: better kill chan?
// TODO: reconnect logic: could try to reconnect for a time, or a new server.
// for now, stopping on errors.
func (c *Client) listen() {
	c.Conn.SetReadDeadline(time.Time{}) // reset read deadline... to infinity!!!
	for c.IsRunning {
		data, err := c.listenOnce()
		if err == nil {
			go handlePacket(data, c.PkChan)
		} else {
			c.Stop()
			break
		}
	}
}

func (c *Client) listenOnce() ([]byte, error) {
	buf := make([]byte, 2056)
	rd, _, err := c.Conn.ReadFromUDP(buf)
	if err != nil {
		log.Println("bad packet: ", err)
		return nil, err
	}
	return buf[:rd], nil
}

// queueHeartbeat queues up a heartbeat message
// TODO: dups OrcaConn
// NOTE: might be worth having OrcaConn do both, client mode flag
func (c *Client) queueHeartbeat() {
	em := om.EmptyMessage{}
	h := om.NewOrcaMessage(om.TagHeartbeat, &em)
	c.PkTrk.QueueMessage(h)
}

// handlePacket receives a packet. This is acting like the server logic
// before letting the connection handle the actual content and ack system
func handlePacket(buf []byte, ch chan<- *om.OrcaPacket) error {
	pk := om.OrcaPacket{}
	if _, err := pk.UnmarshalBinary(buf); err != nil {
		log.Println("bad packet unmarshal: ", err)
		return err // invalid overall packet
	}

	// validate
	if !pk.IsValidChecksum() {
		log.Println("bad checksum")
		return errors.New("bad checksum")
	}

	// unmarshal body
	// deserialize body into messages
	if err := pk.UnmarshalBody(); err != nil {
		log.Println("bad unmarshal", err)
		return err
	}

	// send to client's main processing loop
	ch <- &pk
	return nil
}

func (c *Client) handleMessage(msg *om.OrcaMessage) {
	// actual message logic (TODO: consider mapping out like in om)
	switch msg.Tag {
	case om.TagConnectionChallenge:
		// pass: already handled in handshake
	case om.TagConnected:
		// pass: already handled in handshake
	case om.TagJoined:
		wj := msg.Body.(*om.WhaleJoined)
		c.logger.Println("recv TagJoined, Player ID:", wj.ID)
		c.PlayerID = wj.ID // we joined: store our ID
		c.Game.SetPlayer(wj.ID)
		wc := om.NewOrcaMessage(om.TagConfirmJoined, &om.WhaleConfirmJoined{ID: c.PlayerID})
		c.PkTrk.QueueMessage(wc) // send confirmation
	case om.TagConfirmJoined:
		// another whale joined! store it
		wcj := msg.Body.(*om.WhaleConfirmJoined)
		c.logger.Println("recv TagConfirmJoined, Player ID:", wcj.ID)
	case om.TagHeartbeat:
		// pass
	case om.TagServerFull:
		// TODO:
	case om.TagLeft:
		left := msg.Body.(*om.WhaleLeft)
		// TODO: remove this whale!
		c.logger.Println("recv TagLeft:", left.ID)
	case om.TagReady:
		rdy := msg.Body.(*om.WhaleReady)
		c.logger.Println("recv TagReady:", rdy.ID)
	case om.TagStartGame:
		c.logger.Println("recv TagStartGame!!!")
		sg := msg.Body.(*om.StartGame)
		// TODO: guard
		// TODO: decode ids!
		ids := sg.DecodeIDs()
		c.Game.Start(ids)
	case om.TagStopGame:
		c.logger.Println("recv TagStopGame!!!")
		c.Game.Stop()
	default:
		if msg.Tag > om.Reserved {
			c.Game.HandleMessage(msg)
		} else {
			c.logger.Println("recv unhandled tag:", msg.Tag)
		}
	}
}

// handleSignal handles signals, e.g. game input or commands
// and queues up to send to server if applicable
func (c *Client) handleSignal(s *sig.Signal) {
	switch s.Sig {
	case sig.SigSend:
		msg := s.Data.(*om.OrcaMessage)
		c.PkTrk.QueueMessage(msg)
	case sig.SigKill:
		c.IsRunning = false
	default:
		c.logger.Println("sig not handled", s.Sig)
	}
}

func (c *Client) initLog(opts LogOpts) error {
	if !opts.Verbose {
		c.initNoOpLog()
		return nil
	}
	if !opts.UseFile {
		c.initStandardLog()
		return nil
	}
	err := c.initFileLog()
	return err
}

func (c *Client) initStandardLog() {
	c.logger = log.New(os.Stdout, "[client]", log.LstdFlags)
}

func (c *Client) initFileLog() error {
	df := time.Now().UTC().Format(logNameDate)
	name := fmt.Sprintf("client_log_%s.log", df)
	f, err := os.Create(name)
	if err != nil {
		return err
	}
	c.logger = log.New(f, "[client]", log.LstdFlags)
	return nil
}

func (c *Client) initNoOpLog() {
	c.logger = log.New(ioutil.Discard, "[client]", 0)
}
