# TODO:

## Immediate / Main
1. box test
2. matchmaking: accept guid / password (could do ds3 style)
  + this could require something like redis if we want across multiple servers...
  + for now, look at one, but abstract it?
3. containerize server, CI, try it out w/ dep
4. lag Compensation (if needed)
5. consider increasing message rate of OrcaConn to 30?
6. consider altering retry logic to drop those that aren't crucial
  - e.g. individidual snaps?
7. Better ORCA configuration functions
  - log, max / min players, game spawn, etc.
8. better copy of orca message, not just header?
9. logger on client game
10. better windowless mode

## Matchmaking
- need join / invite w/ guid
- better CLI
- force signal (for less players, will be useful for testing at least)
  - min players

## Handshake
- signing and encryption
  - should configure keys / dir in server
  - client flag for accept insecure (e.g. self-signed)
  - eventually, should be real to prevent man in the middle...assuming...
  - for now, maybe client knows trusted keys?
- reconnect w/ salt? or just session id?
  - this could be a separate message even...
- verify that pendings are removed properly...

## Messaging
- OrcaConn reads never stop in some cases? should be killed...
- reads on bad conn in Client? (possibly server) when server is closed... 
  + need to break that loop
- whale left should probably be a signal to pod..., although we do need to bcast the msg
- unit tests
- variable rate: should be a global and overridable by Game

## C# port of messaging
- Message structs and enums
- Sequence Buffer
- Packet Tracker
- Client will have to be slightly different w/ go routine select
  - in general the calls are the same
- CI to build this into a nuget package? so not needed directly in game?
  - or just do it...

## Clean Up tasks
- profile and come up w/ actual mem per client, etc.
- clean up the serailization (use binary.Write for whole struct?)
- clean up matchmaker: don't like the channel send of channel
  - we could register pods in a map w/ ids, send the id instead of chan, then have orca lookup in ocean? ehh...
- uints instead of ints in messages, seq numbers, etc
- unexport as needed

## CI
- run tests every night
- container builds?

## Dockerize
- server should be Dockerized

## Service Discovery
- eventually we need multiple servers
- they should be able to spawn instantly and go down
- we need a service discovery setup, maybe Consul?
- get to this via regular DNS, maybe load balanced
- each Orca server registers and deregisters accordingly

# Buffering (front end game)
- need a jitter buffer (4-5 frams @ 60Hz) for packet clumping across frames
- aim for a bandwidth of 256k to provide solid play w/ latency
  + actually measure
- don't use smoothing: snap updates hard and let the sim extrapolate from there
  + use error offsets to provide a sort of smoothing instead?
  + then reduce the error offsets
- see snapshot compression
- priority accumulator?
