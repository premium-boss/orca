package signal

// TryAccept is a signal message body for attempting to
// find a Pod for an OrcaConn
type TryAccept struct {
	Blowhole  chan Signal
	ReplyChan chan bool
}
