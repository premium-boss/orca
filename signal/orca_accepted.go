package signal

import om "gitlab.com/premium-boss/orca/messaging"

// OrcaAccepted is a signal from Pod to OrcaConn, indicated it
// has been accepted into the Pod
type OrcaAccepted struct {
	ID      uint16
	PodChan chan<- *om.OrcaMessage
}

// NewOrcaAccepted creates a new OrcaAccepted data wrapper
func NewOrcaAccepted(id uint16, ch chan<- *om.OrcaMessage) OrcaAccepted {
	oa := OrcaAccepted{ID: id, PodChan: ch}
	return oa
}
