package signal

// Signal tags
const (
	SigKill int = iota + 1
	SigTryAccept
	SigOrcaAccepted
	SigCheckJoined
	SigGameCountdownOver
	SigPodDone
	SigPodLostPlayer
	SigOrcaDisconnect
	SigSend
	SigBroadcast
)

// Signal is an internal message, similar to the OrcaMessage
// but with no intention of serializing or sending over the network.
type Signal struct {
	Sig  int
	Data interface{}
}

// Kill is a convenience method for a new SigKill message
func Kill() Signal {
	return Signal{SigKill, nil}
}
