package orca

import "gitlab.com/premium-boss/orca/server"

// Run is the main launcher for all of Orca.
// This is the only thing that games using the Orca network should
// have to call (until we add cancellation).
// Wraps Ocean and Server.
func Run(ip string, port int, stop <-chan bool) {
	o := server.NewOcean()
	go o.Flow()
	s := server.NewServer(o.QueueChan)
	go s.Serve(ip, port)
	<-stop
	s.Stop()
	o.Stop()
}
